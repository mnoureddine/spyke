unit Annuaire;

interface

type
  TAnnuaireReq = (AnnReqConnexion=1,AnnReqDeconnexion=2,AnnReqListes=3,AnnReqInscription=4,AnnReqReussi=5,AnnReqEchoue=7);

const
  PORT_ANNURAIRE = 4523;

function AnnuaireReqToInt(pAnnuaireReq : TAnnuaireReq): integer;
function AnnuaireReq(pReq : Integer): TAnnuaireReq;
implementation

function AnnuaireReqToInt(pAnnuaireReq : TAnnuaireReq): integer;
begin
  Result := Integer(pAnnuaireReq);
end;

function AnnuaireReq(pReq : Integer): TAnnuaireReq;
begin
  result := TAnnuaireReq(pReq);
end;


end.
