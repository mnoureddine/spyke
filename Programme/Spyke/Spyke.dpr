program Spyke;

uses
  Forms,
  Princ in 'Princ.pas' {FormPrinc},
  Connexion in 'Connexion.pas' {FormConnexion},
  Donnee in 'Donnee.pas' {Donnees: TDataModule},
  Parametres in 'Parametres.pas' {FormParametres},
  Annuaire in 'Annuaire.pas',
  Utiles in 'Utiles.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.Title := 'Spyke chat';
  Application.CreateForm(TDonnees, Donnees);
  Application.CreateForm(TFormConnexion, FormConnexion);
  Application.Run;
end.
