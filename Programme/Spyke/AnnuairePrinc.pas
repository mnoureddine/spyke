unit AnnuairePrinc;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs,Annuaire, DB, DBTables, Grids, DBGrids, StdCtrls,IniFiles,
  IdBaseComponent, IdComponent, IdTCPServer, JvComponentBase, JvTrayIcon,
  ActnList, Menus;

type
  TFormAnnuairePrnc = class(TForm)
    TableUtilisateurs: TTable;
    DataSourceUtilisateurs: TDataSource;
    DBGridUtilisateurs: TDBGrid;
    GroupBox1: TGroupBox;
    ButtonActiver: TButton;
    CheckBoxActiverDemarrage: TCheckBox;
    IdTCPServerSpykeAnnuaire: TIdTCPServer;
    ButtonDeconnecterToutLeMonde: TButton;
    JvTrayIcon1: TJvTrayIcon;
    PopupMenuTray: TPopupMenu;
    ActionList1: TActionList;
    ActionActiver: TAction;
    ActionDeconnecterToutLeMonde: TAction;
    ActionQuitter: TAction;
    GroupBox2: TGroupBox;
    ListBoxJournal: TListBox;
    CheckBoxBarreDesTaches: TCheckBox;
    GroupBox3: TGroupBox;
    Afficher1: TMenuItem;
    Activer1: TMenuItem;
    N1: TMenuItem;
    Quitter1: TMenuItem;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure IdTCPServerSpykeAnnuaireConnect(AThread: TIdPeerThread);
    procedure IdTCPServerSpykeAnnuaireExecute(AThread: TIdPeerThread);
    procedure CheckBoxActiverDemarrageClick(Sender: TObject);
    procedure ActionQuitterExecute(Sender: TObject);
    procedure ActionDeconnecterToutLeMondeExecute(Sender: TObject);
    procedure ActionActiverExecute(Sender: TObject);
    procedure CheckBoxBarreDesTachesClick(Sender: TObject);
    procedure Afficher1Click(Sender: TObject);
    procedure FormActivate(Sender: TObject);
  private
    { D�clarations priv�es }
  public
    { D�clarations publiques }
    Parametres : record
        Demarrage : Record
            Activer : boolean;
            BarreDesTaches : boolean;
        end;

    end;
    FichierIni : TIniFile;
    procedure ChargerParametres;
    procedure EnregistrerParametres;
    procedure DeconnecterToutLeMonde;
    procedure EcrireJournal(pMessage : string);
  end;

var
  FormAnnuairePrnc: TFormAnnuairePrnc;

implementation

uses IdTCPConnection;

{$R *.dfm}

procedure TFormAnnuairePrnc.ChargerParametres;
begin
  Parametres.Demarrage.Activer := FichierIni.ReadBool('Demarrage','Activer',false);
  Parametres.Demarrage.BarreDesTaches := FichierIni.ReadBool('Demarrage','BarreDesTaches',false);
end;

procedure TFormAnnuairePrnc.EnregistrerParametres;
begin
  FichierIni.WriteBool('Demarrage','Activer',Parametres.Demarrage.Activer);
  FichierIni.WriteBool('Demarrage','BarreDesTaches',Parametres.Demarrage.BarreDesTaches);
end;

procedure TFormAnnuairePrnc.FormCreate(Sender: TObject);
begin
  FichierIni := TIniFile.Create('SpykeAnnuaire.ini');
  ChargerParametres;
  if Not TableUtilisateurs.Exists then
    TableUtilisateurs.CreateTable;
  CheckBoxActiverDemarrage.Checked := Parametres.Demarrage.Activer;
  CheckBoxBarreDesTaches.Checked := Parametres.Demarrage.BarreDesTaches;
  if Parametres.Demarrage.Activer then
    ActionActiverExecute(sender);
end;

procedure TFormAnnuairePrnc.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  EnregistrerParametres;
end;

procedure TFormAnnuairePrnc.DeconnecterToutLeMonde;
var
  i : Integer;
begin
  if IdTCPServerSpykeAnnuaire.Active then
  begin
    for i := 0 to IdTCPServerSpykeAnnuaire.Bindings.Count - 1 do
    begin
      IdTCPServerSpykeAnnuaire.Bindings[i].CloseSocket();
    end;
  end;

  TableUtilisateurs.First;
  while not TableUtilisateurs.Eof do
  begin
    TableUtilisateurs.Edit;
    TableUtilisateurs.FieldByName('Connecte').AsBoolean := false;
    TableUtilisateurs.Post;
    TableUtilisateurs.Next;
  end;
  EcrireJournal('deconnection de tous les utilisateurs');
end;

procedure TFormAnnuairePrnc.IdTCPServerSpykeAnnuaireConnect(
  AThread: TIdPeerThread);
var
  Utilisateur,MotPasse,IPPublic,IPPrive : string;
  PortPublic,PortPrive : integer;
  Reussi : boolean;
  Req : TAnnuaireReq;
begin

  Reussi := false;
  case AnnuaireReq(AThread.Connection.ReadInteger) of
  AnnReqConnexion :
  begin
    Utilisateur := AThread.Connection.ReadLn;
    MotPasse := AThread.Connection.ReadLn;
    IPPublic := AThread.Connection.ReadLn;
    PortPublic := AThread.Connection.ReadInteger;
    IPPrive := AThread.Connection.ReadLn;
    PortPrive := AThread.Connection.ReadInteger;
    TableUtilisateurs.First;
    if TableUtilisateurs.Locate('Utilisateur',Utilisateur,[loCaseInsensitive])then
    begin
      if TableUtilisateurs.FieldByName('MotPasse').AsString = MotPasse then
      begin
         TableUtilisateurs.Edit;
         TableUtilisateurs.FieldByName('IPPublic').AsString := IPPublic;
         TableUtilisateurs.FieldByName('PortPublic').AsInteger := PortPublic;
         TableUtilisateurs.FieldByName('IPPrive').AsString := IPPrive;
         TableUtilisateurs.FieldByName('PortPrive').AsInteger := PortPrive;
         TableUtilisateurs.FieldByName('Connecte').AsBoolean := true;
         TableUtilisateurs.Post;
         AThread.Connection.WriteInteger(AnnuaireReqToInt(AnnReqReussi));
         Reussi := true;
         EcrireJournal('Utilisateur '+Utilisateur+' connect�.');
      end
      else
        EcrireJournal('Mot de passe '+MotPasse+' <> '+TableUtilisateurs.FieldByName('MotPasse').AsString);
    end
    else
      EcrireJournal('Utilisateur '+Utilisateur+' inconnue.');

    if not Reussi then
    begin
      EcrireJournal('Identification de l''utilisateur '+Utilisateur+' �chou�.');
      AThread.Connection.WriteInteger(AnnuaireReqToInt(AnnReqEchoue));
    end;
  end;
  AnnReqInscription :
  begin
    Utilisateur := AThread.Connection.ReadLn;
    MotPasse := AThread.Connection.ReadLn;
    IPPublic := AThread.Connection.ReadLn;
    PortPublic := AThread.Connection.ReadInteger;
    IPPrive := AThread.Connection.ReadLn;
    PortPrive := AThread.Connection.ReadInteger;
    if not TableUtilisateurs.Locate('Utilisateur',Utilisateur,[loCaseInsensitive])then
    begin
      TableUtilisateurs.Insert;
      TableUtilisateurs.FieldByName('Utilisateur').AsString := Utilisateur;
      TableUtilisateurs.FieldByName('MotPasse').AsString := MotPasse;
      TableUtilisateurs.FieldByName('IPPublic').AsString := IPPublic;
      TableUtilisateurs.FieldByName('PortPublic').AsInteger := PortPublic;
      TableUtilisateurs.FieldByName('IPPrive').AsString := IPPrive;
      TableUtilisateurs.FieldByName('PortPrive').AsInteger := PortPrive;
      TableUtilisateurs.FieldByName('Connecte').AsBoolean := true;
      TableUtilisateurs.Post;
      AThread.Connection.WriteInteger(AnnuaireReqToInt(AnnReqReussi));
      Reussi := true;
      EcrireJournal('Inscription de l''utilisateur '+Utilisateur+' r�ussi.');
    end;
    if not Reussi then
    begin
      AThread.Connection.WriteInteger(AnnuaireReqToInt(AnnReqEchoue));
      EcrireJournal('Inscription de l''utilisateur '+Utilisateur+' �chou�.');
    end;
  end;
  end;


end;

procedure TFormAnnuairePrnc.IdTCPServerSpykeAnnuaireExecute(
  AThread: TIdPeerThread);
var
  Req : TAnnuaireReq;
  Utilisateur : string;
  Query : TQuery;
begin
  while Athread.Connection.Connected do
  begin
    Req := AnnuaireReq(AThread.Connection.ReadInteger);
    case Req of
    AnnReqDeconnexion :
    begin
      Utilisateur := AThread.Connection.ReadLn;
      if TableUtilisateurs.Locate('Utilisateur',Utilisateur,[loCaseInsensitive])then
      begin
          TableUtilisateurs.Edit;
          TableUtilisateurs.FieldByName('Connecte').AsBoolean := false;
          TableUtilisateurs.Post;
          EcrireJournal('D�connexion de l''utilisateur '+Utilisateur);
      end;
    end;
    AnnReqListes :
    begin
      Utilisateur := AThread.Connection.ReadLn;
      Query := TQuery.Create(self);
      Query.DataSource := DataSourceUtilisateurs;
      Query.SQL.add('SELECT * FROM Utilisateurs WHERE Utilisateur <>'+QuotedStr(Utilisateur)+' and Connecte = true');
      Query.Open;

      AThread.Connection.WriteInteger(Query.RecordCount);
      while not Query.Eof do
      begin
        AThread.Connection.WriteLn(Query.FieldByName('Utilisateur').asString);
        AThread.Connection.WriteLn(Query.FieldByName('IPPublic').asString);
        AThread.Connection.WriteInteger(Query.FieldByName('PortPublic').AsInteger);
        AThread.Connection.WriteLn(Query.FieldByName('IPPrive').asString);
        AThread.Connection.WriteInteger(Query.FieldByName('PortPrive').AsInteger);
        Query.Next;
      end;
      Query.Close;
      Query.Free;
      
    end;

    end;
  end;
end;

procedure TFormAnnuairePrnc.CheckBoxActiverDemarrageClick(Sender: TObject);
begin
  Parametres.Demarrage.Activer := CheckBoxActiverDemarrage.Checked;
end;

procedure TFormAnnuairePrnc.ActionQuitterExecute(Sender: TObject);
begin
  DeconnecterToutLeMonde;
  IdTCPServerSpykeAnnuaire.Active := false;
  Close;
end;

procedure TFormAnnuairePrnc.EcrireJournal(pMessage: string);
begin
  ListBoxJournal.Items.Insert(0,DateTimeToStr(now)+#9+pMessage);
  if not JvTrayIcon1.ApplicationVisible then
    JvTrayIcon1.BalloonHint('Annuaire Spyke',pMessage);
end;

procedure TFormAnnuairePrnc.ActionDeconnecterToutLeMondeExecute(
  Sender: TObject);
begin
  DeconnecterToutLeMonde;
end;

procedure TFormAnnuairePrnc.ActionActiverExecute(Sender: TObject);
begin
  if not IdTCPServerSpykeAnnuaire.Active then
  begin
    IdTCPServerSpykeAnnuaire.DefaultPort :=PORT_ANNURAIRE;
    IdTCPServerSpykeAnnuaire.Active := true;
    EcrireJournal('Annuaire activ�');
  end
  else
  begin
    DeconnecterToutLeMonde;
    IdTCPServerSpykeAnnuaire.Active := false;
    EcrireJournal('Annuaire D�sactiv�');

  end;

  if IdTCPServerSpykeAnnuaire.Active then
    ActionActiver.Caption := 'D�sactiver'
  else
    ActionActiver.Caption := 'Activer';
end;

procedure TFormAnnuairePrnc.CheckBoxBarreDesTachesClick(Sender: TObject);
begin
    Parametres.Demarrage.BarreDesTaches := CheckBoxBarreDesTaches.Checked;
end;

procedure TFormAnnuairePrnc.Afficher1Click(Sender: TObject);
begin
  JvTrayIcon1.ShowApplication;
end;

procedure TFormAnnuairePrnc.FormActivate(Sender: TObject);
begin
  if Parametres.Demarrage.BarreDesTaches then
    JvTrayIcon1.HideApplication;
end;

end.
