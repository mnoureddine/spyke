unit bitsetunit;
{$H+}

interface
uses Classes, SysUtils, memunit, math;

type
TBitSet = class
  private
    fPointer: PByte;
    fByteCount, fBitCount: integer;
    procedure ensureBuffer(newBitCount:integer; bigger:boolean);
    procedure setByte(idx:integer; b:byte);
    function getByte(idx:integer): byte;
    property Bytes[idx:integer]: byte read getbyte write setbyte;
  public
    constructor create; overload;
    constructor create(bitcount:integer); overload;
    constructor create(copyFrom:TBitSet); overload;
    procedure setBit(idx:integer; val:boolean);
    function getBit(idx:integer): boolean;
    destructor destroy; override;
    function toStringBin: string;
    function Count: integer;
    procedure SetAll;
    procedure ResetAll;
    procedure doAnd(abs:TBitSet);
    function isZero: boolean;
    function getRandomTrueBit: integer;
    function Mem: pointer;
    function MemSize: integer;
    procedure writeToStream(s:TStream);
    procedure readFromStream(s:TStream);
    property Bit[idx:integer]: boolean read getBit write setBit; default;
end;


implementation

function byteNeeded(bitCount:integer): integer;
begin
  result := (bitCount div 8);
  if (bitCount mod 8) > 0 then inc(result);
end;

procedure TBitSet.setByte(idx:integer; b:byte);
var curs: PByte;
begin
  curs := fPointer;
  inc(curs, idx);
  curs^ := b;
end;

function TBitSet.getByte(idx:integer): byte;
var curs: PByte;
begin
  curs := fPointer;
  inc(curs, idx);
  result := curs^;
end;

function TBitSet.isZero: boolean;
var i: integer;
begin
  result := false;
  for i:=0 to byteNeeded(fBitCount)-1 do
    if Bytes[i] <> 0 then exit;
  result := true;
end;

function TBitSet.getRandomTrueBit: integer;
var s: integer;
begin
  s := random(count); // start from this and do a loop
  for result := s to count-1 do
    if bit[result] then exit;
  for result := 0 to s do
    if bit[result] then exit;
  result := -1;
end;

procedure TBitSet.doAnd(abs:TBitSet);
var i: integer;
begin
  if abs.count <> self.count then
    raise exception.create('makeAnd with different bit count');
  for i:=0 to abs.count-1 do
    if not abs[i] then
      bit[i] := false; // TODO can't just AND the bytes??
end;

function TBitSet.Mem: pointer;
begin
  result := fPointer;
end;

function TBitSet.MemSize: integer;
begin
  result := fByteCount;
end;

procedure TBitSet.ensureBuffer(newBitCount:integer; bigger:boolean);
var n, i, old: integer;
begin
  if newBitCount <= fBitCount then exit; // no need to enlarge  
  fBitCount := newBitCount;
  n := byteNeeded(fbitCount);
  if n = fByteCount then exit; // larger, but on the same byte, no need to enlarge
  old := fByteCount;
  if bigger then
    fByteCount := trunc(n * 1.3)  // make it sligtly larger to avoid continue realloc
  else
    fByteCount := n; 
  ReallocMem(fPointer, fByteCount);
  for i:=old to fByteCount-1 do
    Bytes[i] := 0;
  if fPointer = nil then
    raise Exception.create('Can''t ReallocMem on TBitSet.resizeBuffer!');
end;

constructor TBitSet.create;
begin
  fPointer := nil;
  fByteCount := 0;
  fBitCount := 0;
end;

constructor TBitSet.create(bitcount:integer);
begin
  fPointer := nil;
  fByteCount := 0;
  fBitCount := 0;
  ensureBuffer(bitcount, false);
end;

constructor TBitSet.create(copyFrom:TBitSet);
begin
  fByteCount := copyFrom.fByteCount;
  fBitCount := copyFrom.fBitCount;
  ReallocMem(fPointer, fByteCount);
  Move(copyFrom.Mem^, fPointer^, fByteCount);
end;

destructor TBitSet.destroy;
begin
  freemem(fPointer);
  inherited;
end;

procedure TBitSet.setBit(idx:integer; val:boolean);
var
  byteidx, bitidx: integer;
  b: byte;
begin
  byteidx := idx div 8;
  bitidx := idx mod 8;
  ensureBuffer(idx+1, true);
  b := Bytes[byteIdx];
  if val then
    b := b or (1 shl bitidx)
  else
    b := b and (not(1 shl bitidx));
  Bytes[byteIdx] := b;
end;

function TBitSet.getBit(idx:integer): boolean;
var
  byteidx, bitidx: integer;
  b:byte;
begin
  byteidx := idx div 8;
  bitidx := idx mod 8;
  b := Bytes[byteIdx];
  b := b and (1 shl bitidx);
  result := b <> 0;
end;

function TBitSet.toStringBin: string;
var
  s: string;
  i: integer;
begin
  setlength(s, fBitCount);
  for i:=0 to fBitCount-1 do
    if getBit(i) then s[fBitCount-i] := '1'
    else s[fBitCount-i] := '0';
  result := s;
end;

function TBitSet.Count: integer;
begin
  result := fBitCount;
end;

procedure TBitSet.SetAll;
var i: integer;
begin
  for i:=0 to fByteCount-2 do
    Bytes[i] := 255;
  // manually set the bits of the last byte
  for i := max(fBitCount-8, 0) to fBitCount-1 do
    Bit[i] := true;
end;

procedure TBitSet.ResetAll;
var i: integer;
begin
  for i:=0 to fByteCount-1 do
    Bytes[i] := 0;
end;

procedure TBitSet.writeToStream(s:TStream);
begin
  s.writeBuffer(fBitCount, 4);
  s.writeBuffer(fPointer^, byteNeeded(fBitCount));
end;

procedure TBitSet.readFromStream(s:TStream);
var i: integer;
begin
  s.readBuffer(i, 4);
  ensureBuffer(i, false);  
  i := byteNeeded(fBitCount);
  s.readBuffer(fPointer^, i);
end;


end.
