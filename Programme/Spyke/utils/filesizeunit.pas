unit filesizeunit;
{$H+}

interface
{$IFDEF win32}
uses SysUtils, windows;
{$ELSE}
uses SysUtils, classes;
{$ENDIF}

function FileSize64(FileName:string): int64;

function GetSizeDescription(const IntSize:int64): string;

function GetTimeDescription(seconds:int64): string;


implementation

function GetTimeDescription(seconds:int64): string;
begin
  result := '';
  if seconds > 86400 then // days
  begin
    result := result + inttostr(seconds div 86400) + 'd ';
    seconds := seconds mod 86400;
  end;
  if seconds > 3600 then  // hours
  begin
    result := result + inttostr(seconds div 3600) + 'h ';
    seconds := seconds mod 3600;
  end;
  if seconds > 60 then  // minutes
  begin
    result := result + inttostr(seconds div 60) + 'm ';
    seconds := seconds mod 60;
  end;
  if seconds > 0 then
    result := result + inttostr(seconds) + 's ';
  result := trim(result);
end;

function GetSizeDescription(const IntSize:int64): string;
begin
  if IntSize < 1024 then
    result := IntToStr(IntSize) + ' B'
  else if IntSize < (1024*1024) then
    result := FormatFloat('####0.##', IntSize/1024) + ' KiB'
  else if IntSize < (1024*1024*1024) then
    result := FormatFloat('####0.##', IntSize/1024/1024) + ' MiB'
  else if IntSize < (1024*1024*1024*1024) then
    result := FormatFloat('####0.##', IntSize/1024/1024/1024) + ' GiB'
  else if IntSize < (1024*1024*1024*1024*1024) then
    result := FormatFloat('####0.##', IntSize/1024/1024/1024/1024) + ' TiB'
  else
    result := FormatFloat('####0.##', IntSize/1024/1024/1024/1024/1024) + ' PiB';
end;

{$IFDEF win32}   
// return the exact file size for a file. Return zero if the file is not found.
function FileSize64(FileName:string): int64;
var
  SearchRec: TWIN32FindData;
  h: THandle;
begin
  FileName := FileName + #0;
  h := FindFirstFile(@(FileName[1]), SearchRec );
  if h <> 0 then
    result := int64(SearchRec.nFileSizeHigh) shl int64(32) + int64(SearchREc.nFileSizeLow)
    // calculate the size
  else
    result := 0;
  FindClose(h);                                                   // close the find
end;   
{$ELSE}
// return the exact file size for a file. Return zero if the file is not found.
function FileSize64(FileName:string): int64;
//var F: file of byte;
var x: TFileStream;
begin
  x := TFileStream.create(filename, fmOpenRead or fmShareDenyNone);
  result := x.Size;
  x.free;
  //Assign(F, filename);
  //Reset(F);
  //result := FileSize(F);
  //Close(F);
end;
{$ENDIF}


end.
