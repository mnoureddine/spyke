unit directoryscannerunit;
{$H+}

interface
uses Classes, SysUtils;

type
TDirectoryScanner = class
  private
    info: TSearchRec;
    fIsOpen: boolean;
    currPath: string;
    currAttr: word;
    firstDone: boolean;
    function realNext: boolean;
  public
    constructor Create;
    destructor Destroy; override;
    procedure Open(path:string; attr:Word=faAnyFile);
    function next: boolean;
    procedure Close;
    function fileTime: longint;
    function fileSize: int64;
    function fileAttr: longint;
    function fileName: string;
    function isDirectory: boolean;
    property isOpen: boolean read fIsOpen;
end;


implementation

constructor TDirectoryScanner.Create;
begin
  fIsOpen := false;
end;

procedure TDirectoryScanner.Open(path:string; attr:Word=faAnyFile);
begin
  if fIsOpen then
    raise Exception.create('DirectoryScanner already open');
  fIsOpen := true;
  currPath := path;
  currAttr := attr;
  firstDone := false;
end;

function TDirectoryScanner.next: boolean;
begin
  result := realNext;
  if result and ((fileName() = '.') or (fileName() = '..')) then
    result := next;  // skip . and ..
end;

function TDirectoryScanner.realNext: boolean;
begin
  if not fIsOpen then
    raise Exception.create('DirectoryScanner is closed');
  if not firstDone then
  begin
    result := FindFirst(currpath,currattr,Info) = 0;
    firstDone := true;
  end
  else
    result := FindNext(Info) = 0;
end;

procedure TDirectoryScanner.Close;
begin
  if not fIsOpen then
    raise Exception.create('DirectoryScanner already closed');
  FindClose(Info);
  fIsOpen := false;
end;

destructor TDirectoryScanner.Destroy;
begin
  if isOpen then close;
  inherited Destroy;
end;

function TDirectoryScanner.fileTime: longint;
begin
  result := info.time;
end;

function TDirectoryScanner.fileSize: int64;
begin
  result := info.size;
end;

function TDirectoryScanner.fileAttr: longint;
begin
  result := info.attr;
end;

function TDirectoryScanner.fileName: string;
begin
  result := info.name;
end;

function TDirectoryScanner.isDirectory: boolean;
begin
  result:= (info.Attr and faDirectory) = faDirectory;
end;


end.
