unit systemunit;
{$H+}

interface

function LocalUserPath : string;

function LocalUser : string;

function LocalUserAppdata: string;

procedure openFolder(folder:string);


implementation

{$IFDEF linux}
uses sysutils,libc;

function LocalUser: string;
begin
  result := GetEnvironmentVariable('USER');
end;

function LocalUserPath: string;
begin
  result := GetEnvironmentVariable('HOME');
end;

function LocalUserAppdata: string;
begin
  result := GetEnvironmentVariable('HOME');
end;

procedure openFolder(folder:string);
begin
folder := 'xdg-open '+folder;
libc.system(pchar(folder));
end;

{$ELSE}
uses sysutils, windows,ShellAPI;

procedure openFolder(folder:string);
begin
  ShellExecute(0,'open',PChar(folder),nil,nil, SW_SHOWNORMAL);
end;

function LocalUserPath: string;
begin
  result := sysutils.GetEnvironmentVariable('USERPROFILE');
end;

function LocalUser: string;
begin
   result := sysutils.GetEnvironmentVariable('USERNAME');
end;

function LocalUserAppdata: string;
begin
   result := sysutils.GetEnvironmentVariable('APPDATA');
end;

(*
uses glib2

function CalcTicks: cardinal;

var n: TGTimeVal;
begin
  g_get_current_time(@n);
  result := (n.tv_sec)*1000 + (n.tv_usec) div 1000;
  last := result;
end;
*)
{$ENDIF}


end.
