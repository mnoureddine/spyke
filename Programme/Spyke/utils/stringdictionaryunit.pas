unit stringdictionaryunit;
{$H+}

interface

uses Classes, SysUtils; 

type
TStringDictionary = class
  private
    l: array of
      record
        k, v: string 
      end;
  public
    constructor Create;
    destructor Destroy; override;
    procedure put(key:string; data:string);
    function get(key:string): string; overload;
    function get(key, adefault:string): string; overload;
    procedure remove(key:string);
    function count: integer;
    function key(idx:integer): string;
    property Data[key:string]: string read get write put; default;
end;


implementation

function TStringDictionary.key(idx:integer): string;
begin
  result := l[idx].k;
end;

function TStringDictionary.count: integer;
begin
  result := length(l);
end;

procedure TStringDictionary.put(key:string; data:string);
var i, idx: integer;
begin
  if data = '' then 
  begin 
    remove(key); 
    exit; 
  end;
  idx := -1;
  for i:=0 to length(l)-1 do
    if l[i].k = key then 
    begin
      idx := i;
      break;
    end;
  if idx >= 0 then
    // eventually save old value
    l[idx].v := data
  else
  begin
    idx := length(l);
    setlength(l, idx + 1);
    l[idx].k := key;
    l[idx].v := data;
  end;
end;

function TStringDictionary.get(key, adefault: string): string;
begin
  result := get(key);
  if result = '' then
    result := adefault;
end;

function TStringDictionary.get(key:string): string;
var idx, i: integer;
begin
  idx := -1;
  for i:=0 to length(l)-1 do
    if l[i].k = key then 
    begin
      idx := i;
      break;
    end;
  if idx < 0 then result := ''
  else result := l[idx].v;
end;

procedure TStringDictionary.remove(key:string);
var idx, i, x: integer;
begin
  idx := -1;
  for i:=0 to length(l)-1 do
    if l[i].k = key then 
    begin
      idx := i;
      break;
    end;
  if idx >= 0 then 
  begin
    x := length(l) - 1;
    if idx <> x then
      l[idx] := l[x];
    setlength(l, x);
  end;
end;

constructor TStringDictionary.Create;
begin
  setlength(l, 0);
end;

destructor TStringDictionary.Destroy;
begin
  setlength(l, 0);
  inherited;
end;


end.
