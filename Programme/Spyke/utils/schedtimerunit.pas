unit schedtimerunit;
{$H+}

interface

uses Classes, SysUtils;

type 
TSchedTimer = class;

TTimerCallback = procedure(timer:TSchedTimer; param:TObject) of object;

TSchedTimerEntry = class
  interval: double;
  nextRun: TDateTime;
  call: TTimerCallback;
  param: TObject;
end;

TSchedTimer = class
  private
    entries: TList;
    function nearestEntry: TSchedTimerEntry;
  public
    constructor Create;
    destructor Destroy; override;
    procedure registerTimer(call:TTimerCallback; intervalSeconds:integer; param:TObject);
    procedure check;
    function howMuchToWaitInSeconds: integer;
end;


implementation

const SECOND: Double = 1 / (1/24/60/60);

constructor TSchedTimer.Create;
begin
  entries := TList.create;
end;

destructor TSchedTimer.Destroy; 
begin
  while entries.count > 0 do 
  begin
    TObject(entries[0]).free;
    entries.delete(0);
  end;
  entries.free;
  inherited destroy;
end;

procedure TSchedTimer.check;
var
  e: TSchedTimerEntry;
  i: integer;
begin
  // TODO change this: usea  single "nearestRun" date calculated at each
  // execution and at each registerTimer here only check that date against now()
  // this becouse check can be called quite frequently, ie inside a loop
  for i:=0 to entries.count-1 do
  begin
    e := TSchedTimerEntry(entries[i]);
    if e.nextRun <= now() then
    begin
      try
        e.call(self, e.param);
      except
        on e: Exception do
        begin
          //writexln('Exception on timer call: '+e.message );
        end;
      end;
      //else e.nextRun := e.nextRun+e.interval; // for fixed calls
      e.nextRun := now() + e.interval;
    end;
  end;
end;

function TSchedTimer.nearestEntry: TSchedTimerEntry;
var
  e: TSchedTimerEntry;
  i: integer;
  aux: TDateTime;
begin
  result := nil;
  aux := 1000000000;
  for i:=0 to entries.count-1 do
  begin
    e := TSchedTimerEntry(entries[i]);
    if e.nextRun < aux then
    begin
      aux := e.nextRun;
      result := e;
    end;
  end;
end;

function TSchedTimer.howMuchToWaitInSeconds: integer;
var e: TSchedTimerEntry;
begin
  if entries.count = 0 then
    result := 3600
  else
  begin
    e := nearestEntry;
    result := round((e.nextRun - now()) * SECOND);
  end;
end;

procedure TSchedTimer.registerTimer(call:TTimerCallback; intervalSeconds:integer; param:TObject);
var e: TSchedTimerEntry;
begin
  e := TSchedTimerEntry.create;
  e.interval := intervalSeconds / SECOND;
  e.nextRun := now() + e.interval;
  e.call := call;
  e.param := param;
  entries.add(e);
end;


end.
