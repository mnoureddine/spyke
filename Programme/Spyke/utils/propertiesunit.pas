unit propertiesunit;

interface

uses Classes, SysUtils, logunit, stringdictionaryunit;

type
TProperties = class(Tstringdictionary)
  public
    procedure Load(filename:string);
    procedure Save(filename:string);
end;


implementation

procedure TProperties.Load(filename:string);
var
  s: TstringList;
  r: string;
  i, p: integer;
begin
  s := TstringList.create;
  s.loadfromfile(filename);
  for i:=0 to s.count-1 do
  begin
    r := s[i];
    r := trim(r);
    if (r <> '') and (r[1] <> '#') then
    begin
      p := pos('=', r);
      put(trim(copy(r, 1, p-1)), trim(copy(r, p+1, length(r)-p)));
    end;
  end;
  s.free;
end;

procedure TProperties.Save(filename:string);
var
  s: TstringList;
  r: string;
  i: integer;
begin
  s := TstringList.create;
  for i:=0 to count-1 do
  begin
    r := key(i);
    s.Add(r + ' = ' + get(r));
  end;
  s.SaveToFile(filename);
  s.free;
end;


end.
