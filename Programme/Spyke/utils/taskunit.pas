unit taskunit;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, logunit, boundedbuf, exceptionutils, lockunit;

type

TTask = class
  procedure Execute;virtual;abstract;
end;

{ TTaskExecutor }

TTaskExecutor = class(TThread)
  private
    tasks:TMultiThreadQueue;
    procedure performTask(task:TTask);
  public
    constructor create;
    destructor destroy;override;
    procedure addTask(task:TTask);
    procedure execute;override;
end;

{ TTaskPool }

TTaskPool = class
  private
    lock:TLock;
    tasks:TMultiThreadQueue;
    executors:TList;
  public
    constructor Create;
    destructor Destroy;override;
    procedure addTask(task:TTask; immediate:boolean);
    procedure ensureMinWorkers(num:integer);
end;

implementation

type

{ TPooledThread }

TPooledThread = class(TThread)
  pool:TTaskPool;
  procedure performTask(task:TTask);
  constructor create(aPool:TTaskPool);
  destructor destroy;override;
  procedure execute;override;
end;

{ TPooledThread }


procedure TPooledThread.performTask(task: TTask);
begin
try
  try
    log(llInfo, 'Executing task '+task.ClassName);
    task.Execute;
  except
    on e:exception do
      begin
        log(llError, 'Error in task: '+task.ClassName+' '+e.message);
        log(llError, exceptionTrace());
      end;
  end;
finally
  task.free;
end;
end;

constructor TPooledThread.create(aPool: TTaskPool);
begin
  pool:=apool;
  inherited create(true);
end;

destructor TPooledThread.destroy;
begin
  inherited destroy;
end;

procedure TPooledThread.execute;
var t:TTask;
begin
  while true do
  begin
    // get a task
    t:=TTask(pool.tasks.GetItem());
    // perform the task
    performTask(t);
  end;
end;




{ TTaskExecutor }

procedure TTaskExecutor.performTask(task: TTask);
begin
try
  try
    log(llInfo, 'Executing task '+task.ClassName);
    task.Execute;
  except
    on e:exception do
      begin
        log(llError, 'Error in task: '+task.ClassName+' '+e.message);
        log(llError, exceptionTrace());
      end;
  end;
finally
  task.free;
end;
end;

constructor TTaskExecutor.create;
begin
  tasks:=TMultiThreadQueue.Create(10000);
  inherited create(true);
end;

destructor TTaskExecutor.destroy;
begin
  tasks.free;
  inherited destroy;
end;

procedure TTaskExecutor.addTask(task: TTask);
begin
  tasks.PutItem(task);
end;

procedure TTaskExecutor.execute;
begin
  while true do
    performTask(TTask(tasks.GetItem()));
end;

{ TTaskPool }

constructor TTaskPool.Create;
var x:TPooledThread;
begin
  lock:=TLock.create;
  tasks:=TMultiThreadQueue.Create(1000);
  executors:=TList.create;

  lock.Acquire;
  try
    x:=TPooledThread.Create(self);
    executors.Add(x);
    x.Resume;
  finally
    lock.release;
  end;
end;

destructor TTaskPool.Destroy;
begin
  inherited Destroy;
end;

procedure TTaskPool.addTask(task: TTask; immediate: boolean);
begin
  tasks.PutItem(task);
end;

procedure TTaskPool.ensureMinWorkers(num: integer);
var x:TPooledThread;
begin
  lock.Acquire;
  try
    while executors.Count < num do
      begin
        x:=TPooledThread.Create(self);
        executors.Add(x);
        log(llInfo, 'Thread in pool is now: '+inttostr(executors.Count));
        x.Resume;
      end;
  finally
    lock.release;
  end;
end;

end.

