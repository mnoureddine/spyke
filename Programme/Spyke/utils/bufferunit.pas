unit bufferunit;
{$H+}

interface
uses Classes, SysUtils, memunit;

type
TBuffer = class
  private
    fMem: array of byte;
    function getMemory: pointer;
  public
    constructor create; virtual;
    destructor destroy; override;
    function Size: integer;
    procedure add(buf:TBuffer); overload;
    procedure add(buf:pointer; asize:integer); overload;
    procedure copyTo(buf:TBuffer);
    property Memory: pointer read getMemory;
    procedure asport(dest:pointer; asize:integer);
    function toString: string;
    procedure clear;
    procedure setSize(s:integer);
end;


implementation

constructor TBuffer.create;
begin
  setlength(fMem, 0);
end;


destructor TBuffer.destroy;
begin
  setlength(fMem, 0);
  inherited;
end;

procedure TBuffer.setSize(s:integer);
begin
  setlength(fMem, s);
end;

procedure TBuffer.clear;
begin
  setlength(fMem, 0);
end;

function TBuffer.Size: integer;
begin
  result := length(FMem);
end;

procedure TBuffer.add(buf:TBuffer); 
begin
  add(buf.memory, buf.size);
end;

procedure TBuffer.copyTo(buf:TBuffer);
begin
  buf.clear;
  buf.add(self);
end;

(*
procedure TBuffer.addSizedString(s:string);
var z: word;
begin
  if length(s) > 65535 then
    raise Exception.create('Adding a string too big on the buffer: ' + s);
  z := length(s);
  add(@z, sizeof(z));
  add(@s[1], z);
end;
*)

procedure TBuffer.add(buf:pointer; asize:integer);
var le: integer;
begin
  le := size;
  setlength(fMem, le + asize);
  Move(buf^, fMem[le], aSize);
end;

procedure TBuffer.asport(dest:pointer; asize:integer);
var
  n, i: integer;
  tmp: array of byte;
begin
  if asize > size then
    raise Exception.create('Requested more than the avaiable memory');
  if dest <> nil then
    Move(fMem[0], dest^, aSize);
  if aSize = size then
    setlength(fMem, 0)
  else
  begin
    n := size - aSize;
    setlength(tmp, n);
    Move(fMem[aSize], tmp[0], n);
    Move(tmp[0], fmem[0], n);
    setlength(fMem, n);
    setlength(tmp, 0);
  end;
end;

function TBuffer.toString: string;
begin
  result := memToHex(memory, size);
end;

function TBuffer.getMemory: pointer;
begin
  result := @(fMem[0]);
end;


end.
