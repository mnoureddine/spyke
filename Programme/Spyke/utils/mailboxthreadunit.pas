unit mailboxthreadunit;
{$H+}

interface

uses boundedbuf, Classes, SysUtils;


type

TMail = class
  id: string;
  data: TObject;
end;

TMailboxThread = class(TThread)
  private
    fBuf: TMultiThreadQueue;
  protected
    function getMail(timeoutMillis:cardinal): TMail;
  public
    constructor Create(suspended:boolean; maxMailBoxSize:integer=1024); virtual;
    destructor Destroy; override;
    procedure putMail(mail:TMail);
end;


implementation

function TMailboxThread.getMail(timeoutMillis:cardinal): TMail;
begin
  result := TMail(fBuf.GetItem(timeoutMillis));
end;

constructor TMailboxThread.Create(suspended:boolean; maxMailBoxSize:integer);
begin
  inherited Create(suspended);
  fBuf := TMultiThreadQueue.create(maxMailBoxSize);
end;

destructor TMailboxThread.Destroy;
begin
  fBuf.free;
  inherited destroy;
end;

procedure TMailboxThread.putMail(mail:TMail);
begin
  fBuf.putItem(mail);
end;


end.
