unit stringutilunit;
{$H+}

interface

uses Classes, SysUtils; 

type TStringDynArray = array of string;

function Explode(const S, Separator: string; Limit:integer=0): TStringDynArray;
function Join(const strings:TStringDynArray; Separator:string): string;
function cleanAscii(const s:string): string;
function onlyChars(const s:string): string;


implementation

function onlyChars(const s:string): string;
var i: integer;
begin
  result := '';
  for i:=1 to length(s) do
    if ((lowercase(s[i]) <= 'z') and (lowercase(s[i]) >= 'a')) or
        ((s[i] <= '9') and (s[i] >= '0')) then
      result := result + s[i];
end;

function cleanAscii(const s:string): string;
var i: integer;
begin
  result := '';
  for i:=1 to length(s) do
    if (s[i] < #128) and (s[i] > #20) then
      result := result + s[i]
    else
      result := result + '_';
end;

function Join(const strings:TStringDynArray; Separator:string): string;
var i: integer;
begin
  result := '';
  for i:=0 to length(strings)-1 do
  begin
    result := result + strings[i];
    if i < length(strings)-1 then
      result := result + separator;
  end;
end;

function Explode(const S, Separator: string; Limit:integer=0): TStringDynArray;
var
  SepLen: integer;
  F, P: PChar;
  ALen, Index: integer;
begin
  SetLength(Result, 0);
  if (S = '') or (Limit < 0) then
    exit;
  if Separator = '' then
  begin
    SetLength(Result, 1);
    Result[0] := S;
    exit;
  end;
  SepLen := Length(Separator);
  ALen := Limit;
  SetLength(Result, ALen);

  Index := 0;
  P := PChar(S);
  while P^ <> #0 do
  begin
    F := P;
    P := StrPos(P, PChar(Separator));
    if (P = nil) or ((Limit > 0) and (Index = Limit-1)) then
      P := StrEnd(F);
    if Index >= ALen then
    begin
      Inc(ALen, 5); // mehrere auf einmal um schneller arbeiten zu können
      SetLength(Result, ALen);
    end;
    SetString(Result[Index], F, P-F);
    Inc(Index);
    if P^ <> #0 then
      Inc(P, SepLen);
  end;
  if Index < ALen then
    SetLength(Result, Index); // wirkliche Länge festlegen
end;


end.
