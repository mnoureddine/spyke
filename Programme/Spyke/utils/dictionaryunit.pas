unit dictionaryunit;
{$H+}

interface
uses Classes, SysUtils; 

type

TKey = string;

TDictionary = class
private
  l: TStringList; // TODO make a better implementation!!
public
  constructor Create;
  destructor Destroy; override;
  procedure put(key:TKey; data:TObject);
  function get(key:TKey): TObject;
  procedure remove(key:TKey);
  function count: integer;
  function byIndex(idx:integer): TKey;
  property Data[key:TKey]: TObject read get write put; default;
end;


implementation

function TDictionary.byIndex(idx:integer): TKey;
begin
  result := l[idx];
end;

function TDictionary.count: integer;
begin
  result := l.count;
end;

procedure TDictionary.put(key:TKey; data:TObject);
var
  i: integer;
  o: TObject;
begin
  i := l.indexOf(key);
  if i >= 0 then
  begin
    o := l.objects[i];
    l.delete(i);
    o.free;
  end;
  if data <> nil then
    l.addObject(key, data);
end;

function TDictionary.get(key:TKey): TObject;
var i: integer;
begin
  i := l.indexOf(key);
  if i < 0 then
    result := nil
  else
    result := l.objects[i];
end;

procedure TDictionary.remove(key:TKey);
begin
  put(key, nil);
end;

constructor TDictionary.Create;
begin
  l := TStringList.create;
end;

destructor TDictionary.Destroy;
begin
  l.free;
  inherited;
end;


end.
