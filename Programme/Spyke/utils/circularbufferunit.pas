unit circularbufferunit;
{$H+}

interface
uses Classes, SysUtils, math;

type
TCircularBuffer = class
  private
    fData: pbyte;
    fBufferSize: integer; // num bytes avaiable in total  
    fSize: integer;  // current bytes inside the buffer
    fHead: integer;  // start index
    fTail: integer;  // end intex
 public
    function Peek(Buffer:pointer; Count:longint): longint;
    function Read(Buffer:pointer; Count:longint): longint;
    function Write(Buffer:pointer; Count:longint): longint;
    procedure ReadBuffer(Buffer:pointer; Count:longint);
    procedure WriteBuffer(Buffer:pointer; Count:longint);
    constructor Create(BufferSize:integer);
    destructor destroy; override;    
    property size: integer read fsize;
end;


implementation

constructor TCircularBuffer.Create(BufferSize:integer);
begin
  fData := GetMem(BufferSize);
  fBufferSize := BufferSize;
  fSize := 0;
  fHead := 0;
  fTail := 0;
end;

destructor TCircularBuffer.Destroy;
begin
  Freemem(fData);
  inherited;
end;

procedure TCircularBuffer.ReadBuffer(Buffer:pointer; Count:longint);
begin
  if fsize < count then
    raise Exception.create('Not enought data');
  read(buffer, count);
end;

function TCircularBuffer.Peek(Buffer:pointer; Count:longint): longint;
var a, b: integer;
begin
  result := min(fsize, count);
  if result = 0 then exit;
  if fTail + result > fBufferSize then
  begin
    // two pieces
    a := fBufferSize - fTail; // first
    b := result - a; // second
    Move((fData + fTail)^, buffer^, a);
    Move(fData^, (buffer + a)^, b);
  end
  else
    Move((fData + fTail)^, buffer^, result); // one piece
end;

function TCircularBuffer.Read(Buffer:pointer; Count:longint): longint;
var a, b: integer;
begin
  result := min(fsize, count);
  if result = 0 then exit;
  if fTail + result > fBufferSize then
  begin
    // two pieces
    a := fBufferSize - fTail; // first
    b := result - a; // second
    Move((fData + fTail)^, buffer^, a);
    Move(fData^, (buffer + a)^, b);
    fTail := b;
  end
  else
  begin
    // one piece
    Move((fData + fTail)^, buffer^, result);
    inc(fTail, result);
    if fTail = fBufferSize then fTail := 0;
  end;
  dec(fSize, result);
end;

procedure TCircularBuffer.WriteBuffer(Buffer:pointer; Count:longint);
begin
  if fBufferSize - fsize < count then
    raise Exception.create('Not enought space');
  write(buffer, count);
end;

function TCircularBuffer.Write(Buffer:pointer; Count:longint): longint;
var a, b: integer;
begin
  result := min(fBufferSize - fsize, count);
  if result = 0 then exit;
  if fHead + result > fBufferSize then
  begin
    // two pieces
    a := fBufferSize - fHead; // first
    b := result - a; // second
    Move(buffer^, (fData + fHead)^, a);
    Move((buffer + a)^, fData^, b);
    fHead := b;
  end
  else
  begin
    // one piece
    Move(buffer^, (fData+fHead)^, result);
    inc(fHead, result);
    if fHead = fBufferSize then fHead:=0;
  end;
  inc(fSize, result);
end;


end.
