unit exceptionutils; 
{$mode objfpc}{$H+}

interface
uses Classes, SysUtils; 

function exceptionTrace(): string;


implementation

function exceptionTrace(): string;
var
  i: integer;
  p: pointer;
begin
  result := result + BackTraceStrFunc(ExceptAddr) + #10;
  if (ExceptFrameCount > 0) then
    for i:=0 to ExceptFrameCount-1 do
    begin
      p := (ExceptFrames + i)^;
      result := result + BackTraceStrFunc(p) + #10;
    end;
end;


end.
