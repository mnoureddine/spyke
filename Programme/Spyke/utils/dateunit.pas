unit dateunit;
{$H+}

interface
uses Classes, SysUtils, dateutils;

function dtos(date:TDateTime): string;

function stod(s:string): TDateTime;


implementation

function dtos(date:TDateTime): string;
begin
  result := FormatDateTime('yyyymmddhhnnss', date);
end;

function stod(s:string): TDateTime;
var year, month, day, hour, minute, second :word;
begin
  year := strtoint(copy(s, 1, 4));
  month := strtoint(copy(s, 5, 2));
  day := strtoint(copy(s, 7, 2));
  hour := strtoint(copy(s, 9, 2));
  minute := strtoint(copy(s, 11, 2));
  second := strtoint(copy(s, 13, 2));
  result := EncodeDateTime(year, month, day, hour, minute, second, 0);
end;


end.
