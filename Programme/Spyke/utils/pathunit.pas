unit pathunit;

interface

uses
  Classes, SysUtils, systemunit;

var GUI_PATH:string;
    DEFAULT_COMPLETED_PATH:string;
    DEFAULT_PARTIAL_PATH:string;
    SYSTEM_PATH:string;
    LOG_PATH:string;

procedure setupUserPath(path:string);

implementation

var s,x:string;

procedure setupUserPath(path:string);
var x:string;
begin
  x:=IncludeTrailingPathDelimiter(path);

  SYSTEM_PATH := IncludeTrailingPathDelimiter( x+'system');
  LOG_PATH := IncludeTrailingPathDelimiter( x+'logs');
  DEFAULT_COMPLETED_PATH := IncludeTrailingPathDelimiter( x+'completed');
  DEFAULT_PARTIAL_PATH := IncludeTrailingPathDelimiter( x+'partial');
end;

initialization

{$IFDEF linux}
setupUserPath(IncludeTrailingPathDelimiter(LocalUserAppdata)+'.gazzera');
{$ELSE}
setupUserPath(IncludeTrailingPathDelimiter(LocalUserAppdata)+'gazzera');
{$ENDIF}

s:=ExtractFilePath(paramstr(0));

if s='/usr/bin/' then
begin
  GUI_PATH:= '/usr/share/gazzera/';
end
else
begin
  GUI_PATH:= IncludeTrailingPathDelimiter(s+'gui');
end;

ForceDirectories(SYSTEM_PATH);
ForceDirectories(LOG_PATH);
ForceDirectories(DEFAULT_COMPLETED_PATH);
ForceDirectories(DEFAULT_PARTIAL_PATH);


end.

