unit mapunit;
{$H+}

interface

uses Classes, SysUtils; 

type

TKey = TObject;

TValue = TObject;

TMapEntry = record
  key: TKey;
  value: TValue;
end;

TMap = class
  private
    fAutoFree: boolean;
    entries: array of TMapEntry;
    function indexOf(k:TKey): integer;
    procedure delete(idx:integer);
  public
    constructor Create;
    destructor Destroy; override;
    procedure put(key:TKey; data:TValue); // this frees the eventual old data
    function get(key:TKey): TValue;
    procedure remove(key:TKey);
    function count: integer;
    function byIndex(idx:integer): TKey;
    property Data[key:TKey]: TValue read get write put; default;
    property AutoFree: boolean read fAutoFree write fAutoFree;
end;


implementation

function TMap.indexOf(k:TKey): integer;
begin
  for result:=0 to length(entries)-1 do
    if entries[result].key = k then exit;
  result := -1;
end;

function TMap.byIndex(idx:integer): TKey;
begin
  result := entries[idx].key;
end;

function TMap.count: integer;
begin
  result := length(entries);
end;

procedure TMap.delete(idx:integer);
var
  n: integer;
  val: TObject;
begin
  val := entries[idx].value;
  n := length(entries);
  if (idx <> n-1) then 
    // if it's not the last, copy the last on the position idx
    entries[idx] := entries[n-1];
  // remove last
  setlength(entries, n-1);
  if fAutoFree and (val <> nil) then
    val.free;
end;

procedure TMap.put(key:TKey; data:TValue);
var
  i: integer;
  o: TValue;
begin
  i := indexOf(key);
  if data = nil then
  begin
    if i >= 0 then
      delete(i)
  end
  else if i >= 0 then
  begin
    o := entries[i].value;
    entries[i].value := data;
    if fAutoFree and (o <> nil) then
      o.free;
  end
  else
  begin
    i := length(entries);
    setlength(entries, i+1);
    entries[i].key := key;
    entries[i].value := data;
  end;
(*
  if i >= 0 then
  begin
    o := entries[i].value;
    entries[i].value := data;
    o.free;
  end;
  if data <> nil then 
  begin
    i := length(entries);
    setlength(entries, i+1);
    entries[i].key := key;
    entries[i].value := value;
  end
  else
  begin
    // TODO remove element
    n := length(entries);
    if (i >= 0) and (i <> n-1) then 
    begin
      entries[i] := entries[n-1];
      setlength(entries, n-1);
    end;
  end;
*)
end;

function TMap.get(key:TKey): TValue;
var i: integer;
begin
  i := indexOf(key);
  if i < 0 then result := nil
  else result := entries[i].value;
end;

procedure TMap.remove(key:TKey);
begin
  put(key, nil);
end;

constructor TMap.Create;
begin
  fAutoFree:=true;
  setlength(entries, 0);
end;

destructor TMap.Destroy;
var i: integer;
begin
  if fAutoFree then
    for i:=0 to count-1 do
      with entries[i] do
        if value <> nil then value.free;
 setlength(entries, 0);
 inherited;
end;


end.
