unit integerdbunit;
{$H+}

interface
uses Classes, SysUtils;

const UNITSIZE = sizeof(integer);

type
TIntegerDB = class
  private
    f: TFileStream;
    function getVal(idx:integer): integer;
    procedure setVal(idx, val: integer);
  public
    constructor Create(aFilename:TFileName; maxSize:integer); virtual;
    destructor Destroy; override;
    procedure beginFetch(idx:integer);
    function fetch: integer;
    function endFetch: boolean;
    function count: integer;
    property Values[index:integer]: integer read getVal write setVal;default;
end;


implementation

function TIntegerDb.count: integer;
begin
  result := f.size div UNITSIZE;
end;

procedure TIntegerDb.beginFetch(idx:integer);
begin
  f.Seek(idx * UNITSIZE, soBeginning);
end;

function TIntegerDb.fetch: integer;
begin
  f.ReadBuffer(result, UNITSIZE);
end;

function TIntegerDb.endFetch: boolean;
begin
  result := f.Position = f.Size;
end;

function TIntegerDb.getVal(idx:integer): integer;
begin
  f.Seek(idx * UNITSIZE, soBeginning);
  f.ReadBuffer(result, UNITSIZE);
end;

procedure TIntegerDb.setVal(idx, val: integer);
begin
  f.Seek(idx * UNITSIZE, soBeginning);
  f.WriteBuffer(val, UNITSIZE);
end;

constructor TIntegerDb.Create(aFilename:TFileName; maxSize:integer);
begin
  if not FileExists(aFilename) then
    TFileStream.Create(aFilename, fmCreate).free;
  f := TFileStream.Create(aFilename, fmOpenReadWrite);
  f.size := maxSize * UNITSIZE;
end;

destructor TIntegerDb.Destroy;
begin
  f.free;
  inherited destroy;
end;


end.
