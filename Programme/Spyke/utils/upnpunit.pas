unit upnpunit;

interface

uses
  Classes, SysUtils(*, socketunit, logunit, exceptionutils*)

  ,miniupnp,Windows;


type
  TUPNPRedirection = class(TObject)
  private

  protected

  public

    InPort : String;
    OutPort : String;
    InClient : String;
    Protocole : String;
    RemoteHost : String;
    Duration : String;
    Index : String;
    enabled : String;
    Desc : String;
    i : Integer;
    constructor Create;
    destructor Destroy; override;
  published

  end;

  TUPNPRedirectionList = class(TList)
  private
    function Get(Index: Integer): TUPNPRedirection;
    procedure Put(Index: Integer; Item: TUPNPRedirection);
  public
    constructor Create;
    destructor Destroy; override;
    function Add(Item: TUPNPRedirection): Integer;overload;

    function Add(
              pInPort : String;
              pOutPort : String;
              pInClient : String;
              pProtocole : String;
              pRemoteHost : String;
              pDuration : String;
              pIndex : String;
              pEnabled : String;
              pDesc : String;
              pI : Integer): TUPNPRedirection;overload;
    function Add : TUPNPRedirection;overload;
    function Extract(Item: TUPNPRedirection): TUPNPRedirection;
    function First: TUPNPRedirection;
    function IndexOf(Item: TUPNPRedirection): Integer;
    procedure Insert(Index: Integer; Item: TUPNPRedirection);
    function Last: TUPNPRedirection;
    procedure Delete(Index: Integer);
    function Remove(Item: TUPNPRedirection): Integer;
    property Items[Index: Integer]: TUPNPRedirection read Get write Put; default;
    procedure Clear; override;
  end;

  TUpnpServicesLogEvent = procedure(Sender:Tobject;pLog:string)of object;

  TUpnpServices = class
  private
    ok : boolean;
    devices:PUPNPDev;
    urls:TUPNPUrls;
    data:TIGDdatas;
    localIp:string;
    procedure Log(pLog:string);
  public
    OnLog : TUpnpServicesLogEvent;
    UPNPRedirectionList : TUPNPRedirectionList;
    constructor Create;
    destructor Destroy;override;
    function publicIp:string;
    function PrivateIP : string;
    function initialize:boolean;
    function openFirewall(port:word; protocol:string; id:string):boolean;
    procedure closeFirewall(port:word; protocol:string);
    property initialized:boolean read ok;
    procedure ListeRedirection;
end;



implementation

const
  InternetGatewayDevice = 'urn:schemas-upnp-org:device:InternetGatewayDevice:1';
  WANIPConnection = 'urn:schemas-upnp-org:service:WANIPConnection:1';

constructor TUpnpServices.Create;
begin
 OnLog := nil;
 ok:=false;
 UPNPRedirectionList := TUPNPRedirectionList.Create;
end;

function TUpnpServices.initialize: boolean;
var
  i:integer;
  Error : Integer;
  T,iSt : Integer;

begin
  log('UPNP Initializing');
  try

    ZeroMemory(@urls,SizeOf(Urls));
    ZeroMemory(@data,SizeOf(data));

    devices:= upnpDiscover(3000, nil, nil, 0,0,@error);
    if devices<>nil then
    begin
      log('Found a device:'+devices.descURL);
      SetLength(localIp,16);
      i:=UPNP_GetValidIGD(devices, @urls, @data, @localIp[1], length(localIp));
//      T := length(localIp);
//      iST := 0;
      if i>0 then
      begin
        log('Found something like an UPNP IGD');
        ok:=true;
        result:=true;
      end
      else
      begin
         log('No UPNP IGD found :(');
        ok:=false;
        result:=false;
      end;
    end
    else
    begin
      log('No UPNP device found :(');
      ok:=false;
      result:=false;
    end;


  except
    on e: Exception do
    begin
//      log('UPNP failed initialization ' + e.Message + #10 + e.exceptionTrace);
      log('UPNP failed initialization ' + e.Message);
      ok := false;
      result := false;
    end;
  end;
end;


destructor TUpnpServices.Destroy;
begin
  UPNPRedirectionList.Free;
  inherited Destroy;
end;

function TUpnpServices.publicIp: string;
var
  externalIPAddress:ansistring;
  servicetype : pchar;
  ExternalIP : array[0..15]of char;
  pExternalIP : pchar;
  Err : integer;
begin
  if not ok then
    raise exception.create('UPNP not initialized');
//  setlength(externalIP, 16);


  servicetype := @(data.first.servicetype);
  pExternalIP := @(ExternalIP);
  err := UPNP_GetExternalIPAddress(urls.controlURL, servicetype, pExternalIP);
  if Err <> 0 then
    raise exception.create('UPNP error while obtaining external address');

  result:=string(pExternalIP); // magic here



end;


function TUpnpServices.openFirewall(port: word; protocol: string; id: string):boolean;
var
  portstr:string;
  lservicetype : pchar;
  lInPort : pchar;
  lOutPort : pchar;
  lLocalIP : pchar;
  lID : pchar;
  lProtocole : pchar;
  Err : integer;
  lRemoteHost : pchar;
  lLeaseTime : pchar;
begin
  result := true;
  if not ok then
  begin
    raise exception.create('UPNP not initialized');
    result := false;
  end;
  lservicetype := @(data.first.servicetype);
  lInPort := pChar(inttostr(port)+#0);
  lOutPort := pChar(inttostr(port)+#0);
  lLocalIP := pChar(localIp+#0);
  lID := pChar(ID+#0);
  lProtocole := pChar(protocol+#0);
  lRemoteHost := pchar(''+#0);
  lLeaseTime := pchar('0'+#0);

  Err := UPNP_AddPortMapping( urls.controlURL,
                              lservicetype,
                              lOutPort,
                              lInPort,
                              lLocalIP,
                              lID,
                              lProtocole,
                              lRemoteHost,
                              lLeaseTime);


//  portStr := inttostr(port)+#0;
//  id:=id+#0;

//  protocol:=protocol+#0;
//  if UPNP_AddPortMapping(urls.controlURL, @(data.first.servicetype),@portStr[1], @portStr[1], @localIp[1],@id[1], @protocol[1], nil,nil )<>0 then
  if Err <> 0 then
  begin
    result := false;
    
//    raise exception.create('UPNP error while opening the firewall');
  end;

end;

procedure TUpnpServices.closeFirewall(port: word; protocol: string);
var
  portstr:string;
  lservicetype : pchar;
  lInPort : pchar;
  lOutPort : pchar;
  lInCLient : pchar;
  lID : pchar;
  lProtocole : pchar;
  lRemoteHost : pchar;
  lDuration : pchar;
  Err : integer;
begin

  lservicetype := @(data.first.servicetype);
  lInPort := pChar(inttostr(port)+#0);
  lOutPort := pChar(inttostr(port)+#0);
  lInClient := pChar(localIp+#0);
  lProtocole := pChar(protocol+#0);
  lRemoteHost := pchar(''+#0);
  lDuration := pchar('0'+#0);

  if not ok then
    raise exception.create('UPNP not initialized');
  portStr := inttostr(port)+#0;
  protocol:=protocol+#0;
  Err := UPNP_DeletePortMapping(urls.controlURL, lservicetype,lOutPort, lProtocole , lRemoteHost );
  if Err<>0 then
    raise exception.create('UPNP error while closing the firewall');
end;

function TUpnpServices.PrivateIP: string;
begin
  result := localIp;
end;

procedure TUpnpServices.Log(pLog: string);
begin
  if Assigned(OnLog) then
  begin
    OnLog(Self,pLog);
  end;

end;

procedure TUpnpServices.ListeRedirection;
var
(*
	char index[6];
	char intClient[40];
	char intPort[6];
	char extPort[6];
	char protocol[4];
	char desc[80];
	char enabled[6];
	char rHost[64];
	char duration[16];
  *)
  lservicetype : pchar;
  lInPort : array[0..5]of char;
  lOutPort : array[0..5]of char;
  lInClient : array[0..39]of char;
  lProtocole : array[0..3]of char;
  lRemoteHost : array[0..63]of char;
  lDuration : array[0..15]of char;
  lIndex : array[0..5]of char;
  lenabled : array[0..5]of char;
  lDesc : array[0..79]of char;
  i,j : Integer;
  Err : integer;
  pIndex : pchar;
begin

  lservicetype := @(data.first.servicetype);
  lRemoteHost[0] :=#0;
  lenabled[0] :=#0;
  lDuration[0] :=#0;
  lDesc[0] :=#0;
  lInPort[0] :=#0;
  lOutPort[0] :=#0;
  lInClient[0] :=#0;
//  lID := pChar(ID+#0);
  lProtocole[0] :=#0;

//  lLeaseTime := pchar('0'+#0);

  UPNPRedirectionList.Clear;
  i := 1;
	repeat

    pIndex := pChar(IntToStr(i)+#0);
    for j := 0 to 4 do
    begin
      lIndex[j] := pIndex^;
      if pIndex^=#0 then
        break;
      inc(pIndex);

    end;
    Err := UPNP_GetGenericPortMappingEntry(
                                  Urls.controlURL,
		                               lservicetype,
		                               lindex,
		                               lOutPort, lInClient, lInPort,
									   lProtocole, ldesc, lenabled,
									   lRemoteHost, lDuration);
    UPNPRedirectionList.Add(
              lInPort,
              lOutPort,
              lInClient,
              lProtocole,
              lRemoteHost,
              lDuration,
              lIndex,
              lEnabled,
              lDesc,
              I);
    inc(i);


    (*
		snprintf(index, 6, "%d", i);
		rHost[0] = '\0'; enabled[0] = '\0';
		duration[0] = '\0'; desc[0] = '\0';
		extPort[0] = '\0'; intPort[0] = '\0'; intClient[0] = '\0';
		r = UPNP_GetGenericPortMappingEntry(urls->controlURL,
		                               data->first.servicetype,
		                               index,
		                               extPort, intClient, intPort,
									   protocol, desc, enabled,
									   rHost, duration);
		if(r==0)
		/*
			printf("%02d - %s %s->%s:%s\tenabled=%s leaseDuration=%s\n"
			       "     desc='%s' rHost='%s'\n",
			       i, protocol, extPort, intClient, intPort,
				   enabled, duration,
				   desc, rHost);
				   */
			printf("%2d %s %5s->%s:%-5s '%s' '%s' %s\n",
			       i, protocol, extPort, intClient, intPort,
			       desc, rHost, duration);
		else
			printf("GetGenericPortMappingEntry() returned %d (%s)\n",
			       r, strupnperror(r));
		i++;
	}*)
  until(err<>0);
end;

{ TUPNPRedirection }

constructor TUPNPRedirection.Create;
begin
  inherited;

end;

destructor TUPNPRedirection.Destroy;
begin

  inherited;
end;

{ TUPNPRedirectionList }

function TUPNPRedirectionList.Add(Item: TUPNPRedirection): Integer;
begin
  result := inherited Add(Item);
end;

function TUPNPRedirectionList.Add: TUPNPRedirection;
begin
  Result := TUPNPRedirection.Create;
  Add(Result);
end;

function TUPNPRedirectionList.Add(pInPort, pOutPort, pInClient, pProtocole,
  pRemoteHost, pDuration, pIndex, pEnabled, pDesc: String;
  pI: Integer): TUPNPRedirection;
begin
  result := Add;
  result.InPort := pInPort;
  result.OutPort := pOutPort;
  result.InClient := pInClient;
  result.Protocole := pProtocole;
  result.RemoteHost := pRemoteHost;
  result.Duration := pDuration;
  result.Index := pIndex;
  result.Enabled := pEnabled;
  result.Desc :=pDesc;
  result.I :=pI;
end;

procedure TUPNPRedirectionList.Clear;
var
   i : integer;
begin
  for i := 0 to Count -1 do
    Items[i].Free;
  inherited;

end;

constructor TUPNPRedirectionList.Create;
begin
  inherited;

end;

procedure TUPNPRedirectionList.Delete(Index: Integer);
begin
  items[Index].Free;
  inherited delete(Index);
end;

destructor TUPNPRedirectionList.Destroy;
begin
  Clear;
  inherited;
end;

function TUPNPRedirectionList.Extract(Item: TUPNPRedirection): TUPNPRedirection;
begin
  result := inherited Extract(Item);
end;

function TUPNPRedirectionList.First: TUPNPRedirection;
begin
  Result := inherited First;
end;

function TUPNPRedirectionList.Get(Index: Integer): TUPNPRedirection;
begin
  Result := inherited Get(Index);
end;

function TUPNPRedirectionList.IndexOf(Item: TUPNPRedirection): Integer;
begin
  Result := inherited IndexOf(Item);
end;

procedure TUPNPRedirectionList.Insert(Index: Integer; Item: TUPNPRedirection);
begin
  inherited Insert(Index,Item);
end;

function TUPNPRedirectionList.Last: TUPNPRedirection;
begin
  Result := inherited Last;
end;

procedure TUPNPRedirectionList.Put(Index: Integer; Item: TUPNPRedirection);
begin
  inherited Put(Index,Item);

end;

function TUPNPRedirectionList.Remove(Item: TUPNPRedirection): Integer;
begin
  Item.Free;
  Result := inherited Remove(Item);
end;
end.

