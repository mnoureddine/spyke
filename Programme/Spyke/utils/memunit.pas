unit memunit;
{$H+}

interface

uses Classes, SysUtils, szcodebasex;

function memToHex(data:pointer; size:integer): string;
procedure hexToMem(s:string; dest:pointer);

function hexToBase32(s:string):string;

implementation

const hexchars = '0123456789abcdef';
// array [0..15] of char = ('0', '1', '2', '3', '4', '5', '6', '7', '8', '9',  'a', 'b', 'c', 'd', 'e', 'f');

function memToHex(data:pointer; size:integer): string;
var
  i: integer;
  b: byte;
begin
  setlength(result, size*2);
  for i:=0 to size-1 do
  begin
    b := (pbyte(data) + i)^;
    result[i*2 + 1] := hexchars[(b shr 4) + 1];
    result[i*2 + 2] := hexchars[(b and $0F) + 1];
  end;
end;

procedure hexToMem(s:string; dest:pointer);
var
  i, n: integer;
  b1, b2: byte;
begin
  n := length(s) div 2; 
  for i:=0 to n-1 do
  begin
    b1 := pos(s[i*2 + 1], hexchars) - 1;
    b2 := pos(s[i*2 + 2], hexchars) - 1;
    pbyte(dest)^ := (b1 shl 4) + b2;
    inc(dest, 1);
  end;
end;



function Base32Encode(data:pointer; size:integer): String;
const
  alphabet = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ234567';
var
  i: Integer;
  nr: Int64;
  s:string;
begin
  result := '';
  while size >= 5 do  begin
    nr := 0;
    for i := 0 to 4 do
      nr := nr * 256 + pbyte(data+i)^;
    s:='';
    for i := 0 to 7 do begin
      s := alphabet[(nr mod 32) + 1]+s;
      nr := nr div 32;
    end;
    result := result + s;
    dec(size, 5);
    inc(data, 5);
  end;
  nr := 0;
  if size > 0 then begin
    for i := 0 to size-1 do
      nr := nr * 256 + pbyte(data+i)^;
    for i := 0 to 7 do begin
      if nr > 0 then begin
        result := result + alphabet[(nr mod 32) + 1];
        nr := nr div 32;
      end
      else
        result := result + '=';
    end;
  end;
end;

function hexToBase32(s: string): string;
var p:pointer;
    n:integer;
begin
 n:=length(s) div 2;
 GetMem(p, n);
 hexToMem(lowercase(s), p);

 setlength(result, n*10);

 n:=SZFullEncodeBase32(p, @(result[1]), n);

 setlength(result, n);

 Freemem(p);
end;

function Base32Decode(source: String): String;
const
  alphabet = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ234567';
var
  i: Integer;
  nr: Int64;
  p: Integer;
begin
  result := '';
  while length(source) >= 8 do  begin
    nr := 0;
    for i := 8 downto 1 do begin
      p := pos(source[i], alphabet);
      if p > 0 then
        nr := nr * 32 + p-1
      else
        nr := nr * 32;
    end;
    for i := 1 to 5 do begin
      result := result + chr(nr mod 256);
      nr := nr div 256;
    end;
    delete(source, 1, 8);
  end;
end;



end.
