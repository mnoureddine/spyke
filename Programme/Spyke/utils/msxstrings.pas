unit msxstrings;

interface

uses SysUtils, Classes, Types;

{
MSX components
msx_80@hotmail.com
}

// big idea ! :D
// i've put the function both in the component and here..
function makeAscii(s:string): string;
function SubSet(subset, s: string): boolean; overload;
function GetStringFromWord(const s:string; n:integer; separator:char=' '): string;
function GetStringToChar(c:char; s:string): string;
function GetStringFromChar(c:char; s:string): string;
function SubSet(subset, s: string; var Dove:integer): boolean; overload;
function GetWordNumber(s:string ;n:integer): string; overload;
function GetWordNumber(s:string; n:integer; Separator:char): string; overload;
function GetNumberOfWords(s:string; separator:char=' '): integer;
function IsAbbreviation(abb, s:string): boolean;
Function CmpString(s1, s2:string): boolean;
Function GetStringFrom(from:integer; sour:string): string; overload;
Function GetStringFrom(from, leng: integer; sour:string): string; overload;
function TogliChar(s:string; carattere:char): string;
function Chop(var s:string; separator:char=' '): string;
procedure AggiungiChar(TotalLength:integer; c:char; var s:string);
function GetWord(s:string; separator:char; var indice:integer): string;
function UpCaseFirst(s:string): string;
function SeparateString(s:string; separator:char): TStringDynArray;
procedure GetDirectoryFile(Path:string; List:TStrings);

{****************}
{ IMPLEMENTATION }
{****************}
implementation

procedure GetDirectoryFile(Path:string; List:TStrings);
var sr: TSearchRec;
begin
  if path[length(path)] <> '\' then
    path := path + '\';
  path := path + '*.*';
  list.Clear;
  if FindFirst(path, faAnyFile , sr) = 0 then
  begin
    repeat
      if (sr.Attr and faDirectory	) = 0 then
        list.add(sr.Name);
    until FindNext(sr) <> 0;
    FindClose(sr);
  end;
end;

function UpCaseFirst(s:string): string;
begin
  if length(s) < 1 then
  begin
    result := '';
    exit;
  end;
  s[1] := uppercase(s[1])[1];
  result := s;
end;

function IsAbbreviation(abb, s:string): boolean;
var i: integer;
begin
  if length(abb) > length(s) then
  begin
    result := false;
    exit;
  end;
  abb := lowercase(abb);
  s := lowercase(s);
  result := true;
  for i:=1 to length(abb) do
    if abb[i] <> s[i] then
    begin
      result := false;
      exit;
    end;
end;

function CmpString(s1, s2: string): boolean;
var i: integer;
begin
  result := false;
  if length(s1) = length(s2) then
  begin
    for i:=1 to length(s1) do
      if s1[i] <> s2[i] then exit;
    result := true
  end;
end;

function GetStringToChar(c:char; s:string): string;
var indice: integer;
begin
  result := '';
  indice := 1;
  if indice > length(s) then exit;
  while s[indice] <> c do
  begin
    if indice > length(s) then exit;
    result := result + s[indice];
    inc(indice);
  end;
end;

function GetStringFromChar(c:char; s:string): string;
var indice: integer;
begin
  result := '';
  indice := 1;
  if s = '' then exit;
  while s[indice] <> c do
  begin
    inc(indice);
    if indice >= length(s) then
    begin
      result := '';
      exit;
    end;
  end;
  inc(indice);
  while indice <= length(s) do
  begin
    result := result + s[indice];
    inc(indice);
  end;
end;

function GetStringFrom(from, leng: integer; sour:string): string;
begin
  result := copy(sour, from, leng);
end;

function GetStringFrom(from:integer; sour:string): string;
begin
  result := copy(sour, from, length(sour)-from+1);
{
  leng := length(sour) - from + 1;
  s := '';
  for i:=1 to leng do
    s := s + sour[i+from-1];
  result := trim(s);
}
end;

function SubSet(subset, s: string): boolean;
var i: integer;
begin
  result := false;
  for i:=1 to length(s)-length(subset)+1 do
    if (s[i] = subset[1]) and cmpstring(subset, Getstringfrom(i, length(subset), s)) then
    begin
      result := true;
      exit;
    end;
end;

function SubSet(subset, s: string; var Dove:integer): boolean;
var i: integer;
begin
  result := false;
  for i:=1 to length(s)-length(subset)+1 do
    if (s[i] = subset[1]) and cmpstring(subset, Getstringfrom(i, length(subset), s)) then
    begin
      dove := i;
      result := true;
      exit;
    end;
end;

{ GETNUMBEROFWORDS }
function GetNumberOfWords(s:string; separator:char=' '): integer;
var i, i2: integer;
begin
  s := trim(s);
  if s <> '' then
  begin
    i2 := 1;
    for i:=1 to length(s) do
      if (s[i] = separator) and (s[i+1] <> separator) then
        inc(i2);
    result := i2;
  end
  else
    result := 0;
end;

{ GETSTRINGFROMWORD }
function GetStringFromWord(const s:string; n:integer; separator:char=' '): string;
var
  i: integer;
  s1: string;
begin
  s1 := '';
  for i:=n to GetNumberOfWords(s, separator)+n do
    s1 := s1 + GetWordNumber(s, i, separator) + separator;
  s1 := trim(s1);
  result := s1;
end;

{ GETWORDNUMBER }
procedure AggiungiChar(TotalLength:integer; c:char; var s:string);
var i: integer;
begin
  dec(TotalLength, length(s));
  if TotalLength < 1 then exit;
  for i:=1 to TotalLength do
    s := s + c;
end;

function TogliChar(s:string; carattere:char): string;
var i: integer;
begin
//ShowMessage('Toglispazi da testare');
  result := '';
  for i:=1 to length(s) do
    if s[i] <> carattere then
      result := result + s[i];
end;

function GetWordNumber(s:string; n:integer): string;
begin
  result := GetWordNumber(s, n, ' ');
end;

function GetWordNumber(s:string; n:integer; Separator:char): string;
var
  s1: string;
  i, i2: integer;
begin
  s := trim(s);
  if s <> '' then
  begin
    i := 1;
    for i2:=1 to n do
    begin
      s1 := '';
      while (i <= length(s)) and (s[i] <> separator) do
      begin
        s1 := s1 + s[i];
        inc(i);
      end;
      inc(i);
    end;
    result := s1;
  end
  else
    result:='';
end;

function GetWord(s:string; separator:char; var indice:integer): string;
var i: integer;
begin
  result := '';
  if s = '' then exit;
  if GetNumberOfWords(s, separator) = 1 then
  begin
    result := s;
    s := '';
    exit;
  end;
  i := length(s);
  while (s[indice] <> separator) and (indice <= i) do
  begin
    result := result + s[indice];
    inc(indice);
  end;
  inc(indice);
end;

function Chop(var s:string; separator:char=' '): string;
var
  i: integer;
  trovato: boolean;
  r: string;
begin
  result := '';
  if s = '' then exit;
  if GetNumberOfWords(s, separator) = 1 then
  begin
    result := s;
    s := '';
    exit;
  end;
  trovato := false;
  r := '';
  for i:=1 to length(s) do
  begin
    if trovato then
      r := r + s[i]
    else if s[i] = separator then
      trovato := true
    else
      result := result + s[i];
  end;
  s := r;
end;

function SeparateString(s:string; separator:char): TStringDynArray;
var
  i: integer;
  c: char;
  word: string;
begin
  SetLength(result, 0);
  i := 1;
  word := '';
  while i <= length(s) do
  begin
    c := s[i];
    if c = separator then
    begin
      setlength(result, length(result)+1);
      result[length(result)-1] := word;
      word := '';
      // ora scorriamo il resto di separator
      inc(i);
      while (s[i] = separator) and (i <= length(s)) do
        inc(i);
    end
    else
    begin
      word := word + c;
      inc(i);
    end;
  end;
  if word <> '' then
  begin
    setlength(result, length(result)+1);
    result[length(result)-1] := word;
  end;
end;

function makeAscii(s:string): string;
var i: integer;
begin
  result := s;
  for i:=1 to length(result) do
    if (ord(result[i]) < 32) or (ord(result[i]) > 126) then
      result[i] := '_';
end;

(*
                      _     _
                    o' |,=./ `o
                       (o o)
 ------------------ooO--(_)--Ooo-------------------
/                                                  \
|      This program was developed by {MSX}         |
|              nicola@Lugato.org                   |
|             You can reach me at:                 |
|             http://msx.rules.it                  |
|                   18/5/2001                      |
|           ___                      _             |
|       _.-|   |                    ( (            |
|      {   |   |       /\___/\      | |            |
|       "-.|___|       \ o o / ___ _) )            |
|        .__|_|_.     _(  Y  )  \. `O /            |
|        |      |   _((_ `^-' _ /__<  \            |
|      .+|______|_.-|__)`-' (((/   ((_d            |
| On the net, nobody knows you're really a Kat...  |
\_________________________________________________/

*)
end.
