unit singleinstanceunit;

interface

uses Classes, SysUtils, windows;

procedure CheckSingleInstance(id:string);
procedure StopSingleInstance();


implementation

var
  MutexHandle: THandle;

procedure StopSingleInstance();
begin
CloseHandle(MutexHandle);
end;

procedure CheckSingleInstance(id:string);
var  hPrevInst: Boolean;
begin
  // -== Check to see if the named mutex object existed before this call ==-
  MutexHandle := CreateMutex(nil, TRUE, pchar(id));
  if MutexHandle <> 0 then
    if GetLastError = ERROR_ALREADY_EXISTS then
    // -== set hPrevInst property and close the mutex handle ==-
    begin
      MessageBox(0, 'An Instance of Gazzera is already running for this installation. If it''s blocked, please kill it from the Task Manager.',
          'Application already running', mb_IconHand);
      hPrevInst := TRUE;
      CloseHandle(MutexHandle);
      Halt; // 'Halt' Is the actual one that prevents a second instance
            // of your app from running.
    end
    else
      // -== indicate no previous instance was found ==-
      hPrevInst := FALSE
  else
    // -== indicate no previous instance was found ==-
    hPrevInst := FALSE;
end;


end.
