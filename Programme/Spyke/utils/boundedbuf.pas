unit boundedbuf;

interface
uses SysUtils, lockunit;

type

PQueueItem = ^TQueueItem;

TQueueItem = record
  data: pointer;
  next: PQueueItem;
end;

TQueue = class
  private
    First: PQueueItem;
    Last: PQueueItem;
    fCount: integer;
  public
    constructor Create;
    destructor Destroy; override;
    function push(data:pointer): integer;
    function pop: pointer;
    property Count: integer read fCount;
end;

TMultiThreadQueue = class
  private
    fCount: integer;
    fQueue: TQueue;
    fEntriesFree: TSemaphore;
    fEntriesUsed: TSemaphore;
    fCriticalMutex: TLock;
    function GetCount: integer;
  public
    destructor Destroy; override;
    constructor Create(maxSize:integer); virtual;
    function PutItem(NewItem: Pointer): integer;
    function GetItem(timeoutMillis:cardinal=INFINITE): Pointer;
  published
    property Count: integer read fCount;
end;


implementation

constructor TQueue.Create;
begin
  First := nil;
  Last := nil;
  fCount := 0;
end;

destructor TQueue.Destroy;
begin
  while fCount > 0 do pop;
  inherited destroy;
end;

function TQueue.push(data:pointer): integer;
var c: PQueueItem;
begin
  new(c);
  c.data := data;
  c.next := nil;
  if last = nil then
  begin
    first := c;
    last := c;
  end
  else
  begin
    last.next := c;
    last := c;
  end;
  inc(fCount);
  result := fCount;
end;

function TQueue.pop: pointer;
var l: PQueueItem;
begin
  if fCount = 0 then
  begin
    result := nil;
    exit;
  end;
  result := First.data;
  dec(FCount);
  if First = Last then
  begin
    dispose(First);
    First := nil;
    Last := nil;
  end
  else
  begin
    l := First;
    First := l.next;
    dispose(l);
  end;
end;

function TMultiThreadQueue.GetCount: integer;
begin
  FCriticalMutex.Acquire;
  try
    result := fCount;
  finally
    FCriticalMutex.Release;
  end;
end;

function TMultiThreadQueue.PutItem(NewItem:Pointer): integer;
begin
  FEntriesFree.Acquire;
  FCriticalMutex.Acquire;
  try
    fqueue.Push(NewItem);
    inc(fCount);
    result:=fCount;
  finally
    FCriticalMutex.Release;
    FEntriesUsed.Release;
  end;
end;

function TMultiThreadQueue.GetItem(timeoutMillis:Cardinal): Pointer;
begin
  result := nil;
  if not FEntriesUsed.Acquire(timeoutMillis) then exit;
  FCriticalMutex.Acquire;
  try
    result := fqueue.Pop;
    dec(fCount);
  finally
    FCriticalMutex.Release;
    FEntriesFree.Release;
  end;
end;

constructor TMultiThreadQueue.create(maxSize:integer);
begin
  FCriticalMutex := TLock.Create;
  FEntriesUsed := TSemaphore.create(0, maxSize);
  FEntriesFree := TSemaphore.create(maxSize, maxSize);
  fqueue := TQueue.Create;
end;

destructor TMultiThreadQueue.Destroy;
begin
  FCriticalMutex.Destroy;
  FEntriesUsed.Destroy;
  FEntriesFree.Destroy;
  fqueue.Destroy;
  inherited Destroy;
end;


end.
