unit timerunit;
{$H+}

interface

function GetTicks: cardinal;

function CalcTicks: cardinal;


implementation

{$IFDEF linux}
uses libc;

var last: cardinal;

function CalcTicks: cardinal;
var n: timeval;
begin
  gettimeofday(n, nil);
  result := (n.tv_sec)*1000 + (n.tv_usec) div 1000;
  last := result;
end;

{$ELSE}
uses mmsystem;

var last: cardinal;

function CalcTicks: cardinal;
begin
  result := timeGetTime();
  last := result;
end;

(*
uses glib2

function CalcTicks: cardinal;

var n: TGTimeVal;

begin
  g_get_current_time(@n);
  result := (n.tv_sec)*1000 + (n.tv_usec) div 1000;
  last := result;
end;
*)
{$ENDIF}

function GetTicks: cardinal;
begin
  result := last;
end;


end.
