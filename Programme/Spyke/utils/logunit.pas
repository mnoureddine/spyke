unit logunit;
{$H+}

interface
uses Classes, SysUtils, pathunit, lockunit;
  
type TLogLevel = (llDebug, llInfo, llWarning, llError, llCritical, llNone);

const LogLevels: array[TLogLevel] of string =
    ('DEBUG    ', 'INFO     ', 'WARNING  ', 'ERROR    ', 'CRITICAL ', 'NONE     ');

procedure log(level:TLogLevel; s:string);
function getLogLevel(s:string): TLogLevel;

var MinLogLevel:TLogLevel = llDebug;
		LogToSysout : boolean = false;


implementation

var stream:TFileStream=nil;
    lock:TLock;

procedure initStream;
begin
  lock.Acquire;
  try
    if stream = nil then
      stream := TFileStream.create(LOG_PATH+ExtractFileName(paramstr(0)) + '_' +
        FormatDateTime('yyyymmddhhnnss', now) + '.log', fmCreate or fmShareDenyNone);
  finally
    lock.Release;
  end;

end;

function getLogLevel(s:string): TLogLevel;
var i: TLogLevel;
begin
  result := llNone;
  for i:= low(TLogLevel) to high(TLogLevel) do
    if LowerCase(trim(LogLevels[i])) = LowerCase(trim(s)) then
    begin
      result := i;
      exit;
    end;
end;

procedure log(level:TLogLevel; s:string);
var t: string;
    st:string;
begin
  if (level >= MinLogLevel) then
  begin
    if stream = nil then initstream;
    t := FormatDateTime('hh:nn:ss',now);
    //writeln(t + ' ' + LogLevels[level] + s);
    st := t + ' ' + LogLevels[level] + ' ' + s;
    s := st+#10;
    stream.write(s[1], length(s));
    if LogToSysout then writeln(st);
  end;
end;


initialization

lock:=TLock.create;

finalization

freeandnil(stream);
lock.free;


end.
