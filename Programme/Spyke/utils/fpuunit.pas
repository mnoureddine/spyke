unit fpuunit;
interface


implementation
{$ifdef cpui386}
var Saved8087CW: Word;

{$ifdef FPC} //FPC do not have this function in its RTL
const Default8087CW = $1332; //according to the FPC site it's the value used in the startup code.
                         
procedure Set8087CW(value:word); oldfpccall; Assembler;
asm
   FLDCW  value
end;
{$endif}


initialization
  { Save the current FPU state and then disable FPU exceptions }
  //writexln('Setting FPU');
  Saved8087CW := Default8087CW;
  Set8087CW($133f); { Disable all fpu exceptions }

finalization
  { Reset the FPU to the previous state }
  Set8087CW(Saved8087CW);
  //log('Resetting OK');
{$ENDIF}
end.
