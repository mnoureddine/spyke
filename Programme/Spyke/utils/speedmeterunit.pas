unit speedmeterunit;
{$H+}

interface

uses Classes, SysUtils, timerunit, syncobjs;

const DELAY = 2000;

type
TSpeedMeter = class
  private
    bytes: int64;
    fspeed: cardinal;
    ftotalBytes: int64;
    flastCheck: cardinal;
  public
    procedure UpdateSpeedMeter;
    property Speed: cardinal read fSpeed;
    property Total: int64 read fTotalBytes write fTotalBytes;
    procedure add(abytes:cardinal);
    procedure init;
    constructor Create;
    destructor Destroy; override;
end;


implementation

destructor TSpeedMeter.Destroy;
begin
  inherited destroy;
end;

constructor TSpeedMeter.Create;
begin
  init;
end;

procedure TSpeedMeter.UpdateSpeedMeter;
var
  i: integer;
  n, delta: cardinal;
begin
    n := CalcTicks;
    delta := n - flastCheck;
    if delta < DELAY then exit;
    //writexln('BYTES: ' + inttostr(bytes) + ' DELTA: ' + inttostr(delta));
    fspeed := int64((bytes * 1000) div delta); // *1000 to convert from bytes/millisecond to bytes/second
    bytes := 0;
    flastCheck := n;
end;

procedure TSpeedMeter.init;
begin
  ftotalbytes := 0;
  bytes := 0;
  fspeed := 0;
  flastCheck := CalcTicks;
end;

procedure TSpeedMeter.add(abytes:cardinal);
begin
  inc(bytes, abytes);
  inc(ftotalbytes, abytes);
end;


initialization


finalization


end.
