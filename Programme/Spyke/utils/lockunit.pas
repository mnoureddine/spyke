unit lockunit;

interface
uses
  Classes, SysUtils, syncobjs
  {$IFDEF win32}
  ,windows;
  {$ELSE}
  ,libc;
  {$ENDIF}

const INFINITE = Cardinal(-1);

type

TLock = class
  private
    cs: syncobjs.TCriticalSection;
  public
    constructor Create;
    destructor Destroy; override;
    procedure Acquire;
    procedure Release;
end;

TMultiReadSingleWriteLock = class
  private
    shareCS: syncobjs.TCriticalSection;
    enter: syncobjs.TCriticalSection;
    exlusive: syncobjs.TCriticalSection;
    event: syncobjs.TEvent;
    shareCount: integer;
  public
    constructor Create;
    destructor Destroy; override;
    procedure AcquireShared;
    procedure ReleaseShared;
    procedure ReleaseExclusive;
    procedure AcquireExclusive;
end;

TSemaphore = class
  private
    {$IFDEF win32}
    fHandle: thandle;
    {$ELSE}
    fHandle: sem_T;
    {$ENDIF}
  public
    constructor Create(initialCount, maxcount:integer);
    function Acquire(timeoutMillis:cardinal=INFINITE): boolean;
    procedure Release;
    destructor Destroy; override;
end;

TEvent = class
  {$IFDEF win32}
  fHandle: thandle;
  {$ELSE}
  fHandle: TSemaphore;
  {$ENDIF}
  constructor Create;
  procedure setEvent;
  procedure resetEvent;
  function Wait(timeoutMillis:cardinal=INFINITE): boolean;
  destructor Destroy; override;
end;

  
implementation

{$IFDEF win32}
constructor TSemaphore.Create(initialCount, maxcount: integer);
begin
  fHandle := CreateSemaphore(nil, initialCount, maxcount, nil);
  if fHandle = 0 then
    raise Exception.create('Semaphore could not be created');
end;

function TSemaphore.Acquire(timeoutMillis:cardinal): boolean;
var res: DWord;
begin
  res := WaitForSingleObject(fHandle, timeoutMillis);
  case res of
    WAIT_TIMEOUT: result := false;
    0 : result := true;
    else raise Exception.create('Semaphore could not be acquired');
  end;
end;

procedure TSemaphore.Release;
begin
  if not ReleaseSemaphore(fhandle, 1, nil) then
    raise Exception.create('Semaphore could not be released');
end;

destructor TSemaphore.Destroy;
begin
  CloseHandle(fHandle);
  inherited destroy;
end;


constructor TEvent.Create;
begin
  fhandle := CreateEvent(nil, true, true, nil);
  if fHandle = 0 then
    raise Exception.create('Event could not be created');
end;

procedure TEvent.setEvent;
begin
  if not windows.SetEvent(fhandle) then
    raise Exception.create('Event could not be set');
end;

procedure TEvent.resetEvent;
begin
  if not windows.ResetEvent(fhandle) then
    raise Exception.create('Event could not be set');
end;

function TEvent.Wait(timeoutMillis:cardinal=INFINITE): boolean;
var res: DWord;
begin
  res := WaitForSingleObject(fHandle, timeoutMillis);
  case res of
    WAIT_TIMEOUT: result := false;
    0 : result := true;
   else raise Exception.create('Semaphore could not be acquired');
  end;
end;

destructor TEvent.Destroy;
begin
  CloseHandle(fHandle);
  inherited Destroy;
end;


{$ELSE}
constructor TSemaphore.Create(initialCount, maxcount: integer);
begin
  if sem_init(fHandle, 0, initialCount) <> 0 then
    raise Exception.create('Semaphore could not be created');
end;

function TSemaphore.Acquire(timeoutMillis:cardinal): boolean;
begin
  if timeoutMillis <> INFINITE then
    raise Exception.create('timed wait on semaphore not supported');
  if sem_wait(fHandle) <> 0 then
    raise Exception.create('Semaphore could not be acquired');
  result := true;
end;

procedure TSemaphore.Release;
begin
  if sem_post(fHandle) <> 0 then
    raise Exception.create('Semaphore could not be released');
end;

destructor TSemaphore.Destroy;
begin
  if sem_destroy(fHandle) <> 0 then
    raise Exception.create('Semaphore could not be destroyed');
  inherited destroy;
end;

constructor TEvent.Create;
begin
  fhandle := TSemaphore.create(0, 1);
end;

procedure TEvent.setEvent;
begin
  raise Exception.create('Not implemented');
end;

procedure TEvent.resetEvent;
begin
  raise Exception.create('Not implemented');
end;

function TEvent.Wait(timeoutMillis:cardinal=INFINITE): boolean;
begin
  raise Exception.create('Not implemented');
end;

destructor TEvent.Destroy;
begin
  raise Exception.create('Not implemented');
  inherited Destroy;
end;
{$ENDIF}

constructor TLock.Create;
begin
  cs := syncobjs.TCriticalSection.create();
end;

destructor TLock.Destroy;
begin
  cs.free;
end;

procedure TLock.Acquire;
begin
  cs.acquire;
end;

procedure TLock.Release;
begin
  cs.release;
end;


constructor TMultiReadSingleWriteLock.Create;
begin
  shareCS := syncobjs.TCriticalSection.create;
  enter := syncobjs.TCriticalSection.create;
  event := syncobjs.TEvent.create(nil, true, true, '');
  shareCount := 0;
end;

destructor TMultiReadSingleWriteLock.Destroy;
begin
  shareCS.free;
  enter.Free;
  event.free;
end;

procedure TMultiReadSingleWriteLock.AcquireShared;
begin
  //  test if we can enter
  enter.Acquire;
  // ok, now increment sharedCount
  shareCS.Acquire;
  inc(shareCount);
  event.ResetEvent; // someone is reading, stop the event
  shareCS.Release;
  // release enter lock
  enter.Release;
end;

procedure TMultiReadSingleWriteLock.ReleaseShared;
begin
  shareCS.Acquire;
  dec(shareCount);
  if shareCount = 0 then
    event.SetEvent; // signal eventual writer all readers have exited
  shareCS.Release;
end;

procedure TMultiReadSingleWriteLock.AcquireExclusive;
begin
 // acquire enter so no more shared can enter
  enter.Acquire;
  event.WaitFor(INFINITE);
end;

procedure TMultiReadSingleWriteLock.ReleaseExclusive;
begin
  // let other readers/writers enter
  enter.Release;
end;


end.
