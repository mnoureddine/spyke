unit Parametres;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ComCtrls;

type
  TFormParametres = class(TForm)
    ButtonAnnuler: TButton;
    OK: TButton;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
  private
    { Déclarations privées }
  public
    { Déclarations publiques }
    procedure Charger;
    procedure Enregistrer;
    function Executer : boolean;
  end;


implementation

{$R *.dfm}

{ TFormParametres }

procedure TFormParametres.Charger;

begin
end;

procedure TFormParametres.Enregistrer;
begin

end;

function TFormParametres.Executer: boolean;
begin
  charger;
  result := ShowModal = mrOK;
  if Result  then
    Enregistrer;

end;

end.
