unit serverunit;
{$H+}

interface
uses Classes, SysUtils, socketunit, abstractsocketunit;

const BUFFER_SIZE = 10240; // input buffer for readable socket

type 

TConnDirection = (cdIncoming, cdOutgoing);

TServer = class
  private
    a: array of TAbstractSocket;
    fPort: word;
    procedure remSocket(s:TAbstractSocket);
    function getSocket(index:integer): TAbstractSocket;
    procedure checkSocketsFromReadSet(var readSet:TSocketSet);
    procedure checkSocketsFromWriteSet(var writeSet:TSocketSet);
    procedure addSocketsToReadSet(var readSet:TSocketSet; var maxSocket:TSocket);
    procedure addSocketsToWriteSet(var writeSet:TSocketSet; var maxSocket:TSocket);
    procedure addSocket(s:TAbstractSocket);
  public
    constructor Create(listenAddr:string; aport:word); virtual;
    procedure service(timeOutMillis:longint=-1);
    function connectTo(host:string; port:word; timeout:integer=0): boolean;
    procedure close(s:TAbstractSocket);
    procedure onWriteable(socket:TAbstractSocket); virtual;
    procedure onReadable(socket:TAbstractSocket); virtual; abstract;
    procedure onConnect(socket:TAbstractSocket; address:TAddress; direction:TConnDirection); virtual; abstract;
    procedure onDisconnect(socket:TAbstractSocket); virtual; abstract;
    function newSocket:TAbstractSocket; virtual; abstract;
    function count: integer;
    property Sockets[index:integer]: TAbstractSocket read getsocket; default;
    property Port: word read fPort;
end;

const ConnDirectionStr: array[TConnDirection] of string = ('Incoming', 'Outgoing');

procedure service(servers:array of TServer; timeOutMillis:longint=-1);


implementation

function TServer.connectTo(host:string; port:word; timeout:integer=0): boolean;
var
  s: TAbstractSocket;
  ip: TIpAddress;
begin
  ip := resolveName(host);
  s := newSocket;
  if timeout = 0 then
    result := s.Connect(ip, port)
  else
    result := s.Connect(ip, port, timeout);
  if result then
  begin
    addSocket(s);
    onConnect(s, AddrOfSock(s.FD), cdOutgoing);
  end
  else
    s.free;
end;

constructor TServer.Create(listenAddr:string; aport:word);
begin
  setlength(a,1);
  a[0] := newSocket;
  a[0].bind(getaddr(listenAddr, aport));
  a[0].Listen;
  fport := aport;
end;

function TServer.count: integer;
begin
  result := length(a) - 1;
end;

function TServer.getSocket(index:integer): TAbstractSocket;
begin
  result := a[index+1];
end;

procedure TServer.addSocketsToReadSet(var readSet:TSocketSet; var maxSocket:TSocket);
var i, fd: integer;
begin
  for i:=0 to length(a)-1 do
  begin
    fd := a[i].FD;	
    socketSetAdd(fd, readSet);
    if fd > maxSocket then maxSocket := fd;
  end;
end;

procedure TServer.addSocketsToWriteSet(var writeSet:TSocketSet; var maxSocket:TSocket);
var i, fd:integer;
begin
  for i:=0 to length(a)-1 do
    if a[i].checkWriteable then
    begin
       fd := a[i].FD;	
       socketSetAdd(fd, writeSet);
       if fd > maxSocket then maxSocket := fd;
    end;
end;

procedure TServer.checkSocketsFromReadSet(var readSet:TSocketSet);
var
  i, q: integer;
  s: TAbstractSocket;
  buffer: array[0..BUFFER_SIZE-1] of byte;
  addr: TAddress;
begin
  for i:=length(a)-1 downto 0 do
    if socketSetTest(a[i].FD, readSet) then
      if i = 0 then
      begin
        s := a[0].accept(addr);
        if s <> nil then
        begin
          addSocket(s);
          onConnect(s, addr, cdIncoming);
        end;
      end
      else
        onReadable(a[i]);
(*
        q := a[i].read(@buffer, BUFFER_SIZE);
        if q > 0 then
          onReadData(a[i], @buffer, q)
        else
          close(a[i]);
*)
end;

procedure TServer.checkSocketsFromWriteSet(var writeSet:TSocketSet);
var
  i: integer;
  s: TAbstractSocket;
begin
  for i:=length(a)-1 downto 0 do
    if a[i].checkWriteable and socketSetTest(a[i].FD, writeSet) then
      onWriteable(a[i]);
end;

procedure service(servers:array of TServer; timeOutMillis:longint=-1);
var
  rs, ws: TSocketSet;
  i,res :integer;
  m: TSocket;
begin
  socketSetInit(rs);
  socketSetInit(ws);
  m := 0;
  for i:=0 to length(servers)-1 do
  begin
    servers[i].addSocketsToReadSet(rs, m);
    servers[i].addSocketsToWriteSet(ws, m);
  end;
  res := socketSelect(m+1, rs, ws, TSocketSet(nil^), timeOutMillis);
  //writexln('Result socketSelect: ', res);
(*
  if res = 0 then
  begin
    //writexln('Timeout avvenuto');
  end
*)
  if res = -1 then
    raise Exception.create('Error on socketSelect.')
  else if res <> 0 then
    for i:=0 to length(servers)-1 do
    begin
      servers[i].checkSocketsFromReadSet(rs);
      servers[i].checkSocketsFromWriteSet(ws);
    end;
end;

procedure TServer.service(timeOutMillis:longint=-1);
var
  rs, ws: TSocketSet;
  res: integer;
  m, s: TSocket;
begin
  socketSetInit(rs);
  socketSetInit(ws);
  m := 0;
  addSocketsToReadSet(rs, m);
  addSocketsToWriteSet(ws, m);
  res := socketSelect(m+1, rs, ws, TSocketSet(nil^), timeOutMillis);
(*
  if res = 0 then
  begin
  end
*)
  if res = -1 then
    raise Exception.create('Error on socketSelect.')
  else if res <> 0 then
  begin
    checkSocketsFromReadSet(rs);
    checkSocketsFromWriteSet(ws);
  end;
end;

procedure TServer.close(s:TAbstractSocket);
begin
  // note: you have to call server.close(socket) to close a socket, not just socket.close as it can cause problems: the following "select" would be called on a closed socket
  remSocket(s);
  onDisconnect(s);
  s.close;
  s.free;
end;

procedure TServer.addSocket(s:TAbstractSocket);
var l, i: integer;
begin
  l := length(a);
  setlength(a, l+1);
  a[l] := s;
end;

procedure TServer.remSocket(s:TAbstractSocket);
var l, i, i2: integer;
begin
  l := length(a);
  for i:=0 to l-1 do
    if a[i]=s then
    begin
      for i2 := i to l-2 do
        a[i2] := a[i2+1];
      setlength(a, l-1);
      exit;
    end;
end;

procedure TServer.onWriteable(socket:TAbstractSocket);
begin

end;


end.
