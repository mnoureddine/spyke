unit socketunit;

interface
{$H+}

uses Classes, SysUtils,logunit,
    {$IFDEF win32}
    winsock
    {$ELSE}
    libc, initc
    {$ENDIF}
    ;

type

TAddress = TSockAddrIn;
TIpAddress = TInAddR;
TSocket = integer;
TSocketSet = Tfdset;

// socket functions
function socketCreate(): TSocket;
procedure socketBind(fSocket:TSocket; Addr:TAddress); overload;
procedure socketBind(fSocket:TSocket; port:word); overload;
function socketSend(fSocket:TSocket; buffer:pointer; length:integer): longint;
function socketRecv(fSocket:TSocket; buffer:pointer; maxlength:integer): longint;
procedure setBlocking(fSocket:TSocket; block:boolean);
function socketAccept(fSocket:TSocket): TSocket; overload;
function socketAccept(fSocket:TSocket; var address:TAddress): TSocket; overload;
procedure socketListen(fSocket:TSocket);
function socketConnect(fSocket:TSocket; ip:TIpAddress; port:word): boolean; overload;
function socketconnect(fSocket:TSocket; ip:TIpAddress; port:word; timeout:integer): boolean; overload;
procedure socketClose(fSocket:TSocket);
function getSocketError: longint;
function resolveName(name:string): TIpAddress;

// select functions
procedure socketSetInit(var SocketSet:TSocketSet);
procedure socketSetAdd(s:TSocket; var SocketSet:TSocketSet);
procedure socketSetDel(s:TSocket; var SocketSet:TSocketSet);
function socketSetTest(s:TSocket; var SocketSet:TSocketSet): boolean;
function socketSelect(nfds:longint; var readSet:TSocketSet; var writeSet:TSocketSet; var exceptSet:TSocketSet; timeoutMillis:longint=-1): longint;

// address utility functions
function getaddr(ip:string; port:word): TAddress;
function InAddrToIp(s:TIpAddress): string;
function IpToInAddr(const s:string): TInAddr;
function AddrToStr(const addr:TAddress): string;
function AddrOfSock(const s:TSocket): TAddress;
function localIp(): TIpAddress;

function validateHost(host:string; addr:TIpAddress): boolean;
function isAddressLocal(a:TIpAddress): boolean; overload;


implementation

type clong = {$IFDEF win32}longint{$ELSE}longword{$ENDIF};
(*
    inttostr(ord(s_b1)) + '.' +
    inttostr(ord(s_b2)) + '.' +
    inttostr(ord(s_b3)) + '.' +
    inttostr(ord(s_b4));
*)

function localIp(): TIpAddress;
var s: string;
begin
  setlength(s, 256);
  if gethostname(@s[1], 256) = SOCKET_ERROR then
    raise Exception.create('Error on gethostname');
  result := resolveName(s);
end;

function isAddressLocal(a:TIpAddress): boolean;
var b1, b2: byte;
begin
  result := true;
  b1 := ord(a.S_un_b.s_b1);
  b2 := ord(a.S_un_b.s_b2);
  case b1 of
    10: exit;
    127: exit;
    192: if b2 = 168 then exit;
    172: if (b2 >= 16) and (b2 <= 32) then exit;
  end;
  result := false;
end;

function validateHost(host:string; addr:TIpAddress): boolean;
begin
  result := InAddrToIp(resolveName(host)) = InAddrToIp(addr);
end;

procedure socketSetInit(var SocketSet:TSocketSet);
begin
  FD_ZERO(socketSet);
end;

procedure socketSetAdd(s:TSocket; var SocketSet:TSocketSet);
begin
  FD_SET(s, socketSet);
end;

procedure socketSetDel(s:TSocket; var SocketSet:TSocketSet);
begin
  FD_CLR(s, socketSet);
end;

function socketSetTest(s:TSocket; var SocketSet:TSocketSet): boolean;
begin
  result:=FD_ISSET(s, socketSet);
end;

function socketSelect(nfds:longint; var readSet:TSocketSet; var writeSet:TSocketSet; var exceptSet:TSocketSet; timeoutMillis:longint=-1): longint;
{$IFDEF win32}
var t: TTimeVal;
begin
  if timeOutMillis = -1 then
    result:=select(nfds, @readSet, @writeSet, @exceptSet, nil)
  else
  begin
    t.tv_sec := timeOutMillis div 1000;
    t.tv_usec := (timeOutMillis mod 1000) * 1000;
    result := select(nfds, @readSet, @writeSet, @exceptSet, @t);
  end;
end;
{$ELSE}
var t: TTimeVal;
begin
  if timeOutMillis = -1 then
    result := select(nfds, readSet, writeSet, exceptSet, Timeval(nil^))
  else
  begin
    t.tv_sec := timeOutMillis div 1000;
    t.tv_usec := (timeOutMillis mod 1000) * 1000;
    result := select(nfds, readSet, writeSet, exceptSet, t)
  end;
end;
{$ENDIF}

function AddrOfSock(const s:TSocket): TAddress;
var i: clong;
begin
  i := Sizeof(result);
  if GetPeerName(s, result, i) <> 0 then
  begin
    log(llError, 'Error GetPeerName ' + inttostr(getSocketError));
    result.sin_addr := IpToInAddr('0.0.0.0');
    result.sin_port := 0;
  end;
end;

function AddrToStr(const addr:TAddress): string;
begin
  //writexln('port ', addr.sin_port, ' ->  ', NToHs(addr.sin_port));
  result := InAddrToIp(addr.sin_addr);
  result := result + ':' + inttostr(NToHs( addr.sin_port));
end;

function IpToInAddr(const s:string): TInAddr;
var
  i, num: integer;
  c: char;
  word: string;
  b: array[0..3] of byte;
begin
  i := 1;
  word := '';
  num := 0;
  while i <= length(s) do
  begin
    c := s[i];
    if c = '.' then
    begin
      b[num] := strtoint(word);
      inc(num);
      word := '';
      inc(i);
    end
    else
    begin
      word := word + c;
      inc(i);
    end;
  end;
  if word <> '' then
    b[num] := strtoint(word);
  result.s_addr := (b[0]shl 24) or (b[1]shl 16) or (b[2]shl 8) or (b[3]);
  result.s_addr := htonl(result.s_addr );
  //writexln(inaddrToip(result));
end;

function InAddrToIp(s:TIpAddress): string;
begin
with s.S_un_b do
  result := inttostr(ord(s_b1)) + '.' + inttostr(ord(s_b2)) + '.' +
      inttostr(ord(s_b3)) + '.' + inttostr(ord(s_b4));
log(llInfo, 'InAddrToIp returned: ' + result);
end;


procedure FillByte(P :PByte;T : integer;V : byte);
begin
  while T>0 do
  begin
    P^ := V;
    inc(P);
    dec(T);
  end;
end;
function getaddr(ip:string; port:word): TAddress;
begin
  result.sa_family := AF_INET;
  result.sin_addr := IpToInAddr(ip);
  //writexln('addr: ' + inttostr(integer(result.sin_addr)));
  result.sin_port := htons(port);
  FillByte(PByte(result.sin_zero[0]), sizeof(result.sin_zero), 0);
end;

function socketCreate():TSocket;
begin
 result := socket(AF_INET, SOCK_STREAM, 0);
 if result < 0 then
  raise Exception.create('Error while creating socket');
end;

function socketsend(fSocket:TSocket; buffer:pointer; length:integer): longint;
begin
  {$IFDEF win32}
  result := Send(fsocket, buffer^, length, 0);
  {$ELSE}
  result := Send(fsocket, buffer^, length, MSG_NOSIGNAL); // avoid generating SIGPIPE
  {$ENDIF}
end;

function socketrecv(fSocket:TSocket; buffer:pointer; maxlength:integer): longint;
begin
  result := Recv(fsocket, buffer^, maxlength, 0);
(*
  case result of
    -1:
      case getSocketError of
        11, 10035: // EAGAIN: timeout o would block su linux, WSAEWOULDBLOCK su windows
        begin
          // indicano che la chiamata e' uscita perche' non ci sono dati e il socket e' nonblocking.
          writexln('EAGAIN or WSAEWOULDBLOCK');
          result := 0;
        end;
        104: // ECONNRESET Connection reset by peer
        begin
          writexln('ECONNRESET');
          result := 0;
        end;
        else
          raise exception.create('error receiving from socket. sockError: ' + inttostr(getSocketError));
      end;
    0:
      // the socket was gracefully closed.
      fsocket := -1; // this will block further recv
  end;
*)
end;

function socketconnect(fSocket:TSocket; ip:TIpAddress; port:word): boolean;
var
  v: TAddress;
  i: integer;
begin
  FillByte(pByte(@v), sizeof(v), 0);
  v.sa_family := AF_INET;
  v.sin_addr := ip;
  v.sin_port := htons(port);
  i := Connect(fsocket, v, sizeof(v));
  result := i = 0;
end;

function getSocketError: longint;
begin
  {$IFDEF linux}
  result := cerrno;
  {$ELSE}
  result := WSAGetLastError;
  {$ENDIF};
end;

function socketconnect(fSocket:TSocket; ip:TIpAddress; port:word; timeout:integer): boolean;
var
  v: TAddress;
  i: integer;
  ss: TSocketSet;
  lon, valopt: integer;
begin
  result := false;
  FillByte(@v, sizeof(v), 0);
  v.sa_family := AF_INET;
  v.sin_addr := ip;
  v.sin_port := htons(port);
  setBlocking(fsocket, false);
  i := Connect(fsocket, v, sizeof(v));
  if i = 0 then
  begin
    result := true;
    exit;
  end;
  //writexln('connect returned: ', i);
  {$IFDEF win32}
  //writexln(getSocketError , ' <> ',  EINPROGRESS, '-', WSAEINPROGRESS, '-', WSAEWOULDBLOCK);
  if (getSocketError <> 0) and (getSocketError <> WSAEWOULDBLOCK) then
  {$else}
  //writexln(getSocketError , ' <> ',  EINPROGRESS);
  if getSocketError <> EINPROGRESS then
  {$ENDIF}
  begin
    socketClose(fsocket);
    exit;
  end;

  socketSetInit(ss);
  socketSetAdd(fsocket, ss);

  i := socketSelect(fsocket+1, TSocketSet(nil^), ss, TSocketSet(nil^), timeout);
  if i < 0 then
  begin
    // error
    socketClose(fsocket);
    exit;
  end
  else if i = 0 then
  begin
    // timeout
    socketClose(fsocket);
    exit;
  end
  else if i > 0 then
  begin
    {$IFDEF win32}
    // no problem, if we get here it should be connected
    //writexln('OK!');
    {$ELSE}
    // linux require checking socket error ?
    lon := sizeof(integer);
    getsockopt(fsocket, SOL_SOCKET, SO_ERROR, @valopt, @lon);
    if valopt <> 0 then
    begin
      // fprintf(stderr, "Error in connection() %d - %s\n", valopt, strerror(valopt));
      socketClose(fsocket);
      exit;
    end;
    {$ENDIF}
  end;

  //writexln('Setting bloking');
  setBlocking(fsocket, true);
  //writexln('Setting bloking.. OK');
  result := true;
end;

procedure socketbind(fSocket:TSocket; port:word);
var v: TAddress;
begin
  v.sa_family := AF_INET;
  v.sin_addr := TInAddr(htonl(INADDR_ANY));
  v.sin_port := htons(port);
  socketbind(fSocket, v);
end;

function socketaccept(fSocket:TSocket; var address:TAddress): TSocket;
var
  l: clong;
  s: longint;
begin
  l := sizeof(address);
  {$IFDEF win32}
  s := Accept(fsocket, address, PSOCKADDR(@l));
  {$ELSE}
  s := Accept(fsocket, address, l);
  {$ENDIF}
  if s = -1 then
    raise exception.create('Error while accepting connection');
  result := s;
end;

function socketaccept(fSocket:TSocket): TSocket;
var v: TAddress;
begin
  result := socketaccept(fsocket, v);
end;

procedure socketlisten(fSocket:TSocket);
begin
  if Listen(fsocket, 10) <> 0 then
    raise exception.create('Error while listening');
end;

function socketRecvString(fSocket:TSocket): string;
var c: char;
begin
  result := '';
  while socketRecv(fSocket, @c, 1) = 1 do
  begin
    if c = #10 then break;
    result := result + c;
  end;
  result := trim(result);
end;

function socketsendString(fSocket:TSocket; s:string): longint;
begin
  result := socketsend(fSocket, @s[1], length(s));
end;

procedure socketbind(fSocket:TSocket; Addr:TAddress);
var optval: integer;
begin
  optval := 1;
  {$IFNDEF win32}
  if setsockopt(fSocket, SOL_SOCKET, SO_REUSEADDR, @optval, sizeof(optval)) <> 0 then
    raise Exception.create('Error setting socket oprions');
  {$ENDIF}
  if Bind(fsocket, Addr, sizeof(addr)) <> 0 then
    raise Exception.create('Error while binding socket');
end;

(*
type

PAddrInfo = ^TAddrInfo;

TAddrInfo = record
  ai_flags: integer;    // AI_PASSIVE, AI_CANONNAME, AI_NUMERICHOST.
  ai_family: integer;   // PF_xxx.
  ai_socktype: integer; // SOCK_xxx.
  ai_protocol: integer; // 0 or IPPROTO_xxx for IPv4 and IPv6.
  ai_addrlen: u_int;    // Length of ai_addr.
  ai_canonname: PChar;  // Canonical name for nodename.
  ai_addr: PSockAddr;   // Binary address.
  ai_next: PAddrInfo;     // Next structure in linked list.
end;

function getaddrinfo(NodeName: PChar; ServName: PChar; Hints: PAddrInfo; var Addrinfo: PAddrInfo): integer; stdcall; external 'ws2_32.dll';
function freeaddrinfo(Addrinfo: PAddrInfo): integer; stdcall; external 'Ws2_32.dll';
*)

function resolveName(name:string): TIpAddress;
var res: phostent;
begin
  name := name + #0;
  res := gethostbyname(@name[1]);
  if res = nil then
    result := IpToInAddr('0.0.0.0')
  else
    result := PInAddr(res^.h_addr^)^;
  //writexln(name, ' -> ', InAddrToIp(result));
end;

procedure socketClose(fSocket:TSocket);
begin
  {$IFDEF linux}
  shutdown(fsocket, SHUT_RDWR);
  fileclose(fsocket);
  {$ELSE}
  CloseSocket(fsocket);
  {$ENDIF}
  fsocket := -1;
end;

{$IFDEF LINUX}
procedure setBlocking(fSocket:longint; block:boolean);
var flags: integer;
begin
  flags := fcntl(fsocket, F_GETFL, 0);
  if flags = -1 then flags := 0;
  if block then
    flags := flags AND (not O_NONBLOCK)
  else
    flags := flags or O_NONBLOCK;
  if fcntl(fsocket, F_SETFL, flags) = -1 then
    raise Exception.create('Error while setting socket blockability');
end;
{$ELSE}
procedure setBlocking(fSocket:longint; block:boolean);
var mode: integer;
begin
  //-------------------------
  // Set the socket I/O mode: In this case FIONBIO
  // enables or disables the blocking mode for the
  // socket based on the numerical value of iMode.
  // If iMode = 0, blocking is enabled;
  // If iMode != 0, non-blocking mode is enabled.
  if block then mode := 0
  else mode := -1;
  ioctlsocket(fSocket, FIONBIO, @mode);
end;
{$ENDIF}

{$IFDEF win32}
{ Winsocket stack needs an init. and cleanup code }
var wsadata: twsadata;


initialization
  WSAStartUp($2, wsadata);

  
finalization
  WSACleanUp;
{$ENDIF}
end.
