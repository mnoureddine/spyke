unit abstractsocketunit;
{$H+}

interface
uses socketunit, classes, sysutils;

type

SocketException = class(Exception)
end;

ConnectionClosedException = class(SocketException)
end;

ConnectionErrorException = class(SocketException)
end;

WouldBlockException = class(SocketException)
end;

TAbstractSocket = class
  private
    fData: pointer;
    fCheckWriteable: boolean;
    fStream: TStream;
  public
    procedure readFully(buf:pointer; size:longint);
    procedure writeFully(buf:pointer; size:longint);
    function read(buf:pointer; size:longint): longint; virtual; abstract;
    function write(buf:pointer; size:longint): longint; virtual; abstract;
    function getFD: integer; virtual; abstract;
    procedure close; virtual; abstract;
    function connect(ip:TIpAddress; port:word): boolean; overload; virtual; abstract;
    function connect(ip:TIpAddress; port:word; timeout:integer): boolean; overload; virtual; abstract;
    procedure bind(address:TAddress); virtual; abstract;
    procedure listen; virtual; abstract;
    function accept(var addr:TAddress): TAbstractSocket; virtual; abstract;
    function getAddress: TAddress; virtual; abstract;
    property FD: integer read getFD;
    property Data: pointer read fData write fData;
    property CheckWriteable: boolean read fCheckWriteable write fCheckWriteable;
    property Stream: TStream read fStream;
    constructor Create; virtual;
    destructor Destroy; override;
end;


implementation

type TSocketStream = class(TStream)
  fS: TAbstractSocket;
  function Read(var Buffer; Count:longint): longint; override;
  function Write(const Buffer; Count:longint): longint; override;
end;

function TSocketStream.Read(var Buffer; Count:longint): longint;
begin
  result := count;
  if count > 0 then
  while true do
  try
    fs.readFully(@buffer, count);
    exit;
  except
    on e: WouldBlockException do
      begin
        sleep(10); // sleep a little and retry
      end;
  end;
end;

function TSocketStream.Write(const Buffer; Count:longint): longint;
begin
  result := count;
  if count > 0 then
    fs.writeFully(@buffer, count);
end;

procedure TAbstractSocket.readFully(buf:pointer; size:longint);
var i: integer;
begin
  while size > 0 do 
  begin
    i := read(buf, size);
    if i <= 0 then 
      raise exception.create('Could''t read fully!');
    dec(size, i);
    inc(buf, i);
  end;
end;

procedure TAbstractSocket.writeFully(buf:pointer; size:longint);
begin
  if write(buf, size) <> size then
    raise exception.create('Could''t write fully!');
end;

constructor TAbstractSocket.Create;
begin
  fData := nil;
  fStream := TSocketStream.create;
  TSocketStream(fStream).fs := self;
end;

destructor TAbstractSocket.Destroy; 
begin
  fstream.Free;
  inherited;
end;


end.
