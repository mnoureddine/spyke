unit normalsocketunit;
{$H+}

interface
uses socketunit, abstractsocketunit, sysutils;

type
TNormalSocket = class(TAbstractSocket)
  private
   fSocket: TSocket;
  public
   function read(buf:pointer; size:longint): longint; override;
   function write(buf:pointer; size:longint): longint; override;
   function getFD: integer; override;
   procedure close; override;
   function connect(ip:TIpAddress; port:word): boolean; overload; override;
   function connect(ip:TIpAddress; port:word; timeout:integer): boolean; overload; override;
   procedure bind(address:TAddress); override;
   procedure listen; override;
   function accept(var addr:TAddress): TAbstractSocket; override;
   function getAddress:TAddress; override;
   procedure setFD(afd:integer);
   property FD:integer write setFD;
   constructor Create(aSocket:TSocket); overload; virtual;
   constructor Create; overload; override;
   destructor Destroy; override;
end;


implementation

procedure TNormalSocket.setFD(afd:integer);
begin
  fsocket := afd;
end;

procedure TNormalSocket.close;
begin
  socketClose(fSocket);
end;

function TNormalSocket.read(buf:pointer; size:longint): longint; 
var e: longint;
begin
  if size = 0 then
    ConnectionErrorException.create('size=0!!!');
  result := socketRecv(fSocket, buf, size);
  if result = 0 then
    raise ConnectionClosedException.create('Connection closed (Recv = 0)');
  if result = -1 then
  begin
    e := getSocketError;
    case e of
      11, 10035: // EAGAIN: timeout o would block on linux, WSAEWOULDBLOCK on windows
        // it means the socket is in nonblocking mode and no data is avaiable
        raise WouldBlockException.create('Request of '+inttostr(size)+' byte would block');
      else
        raise ConnectionErrorException.create('Connection error (Recv = -1)');
    end;
  end;
end;

function TNormalSocket.write(buf:pointer; size:longint): longint; 
begin
  if size = 0 then
    ConnectionErrorException.create('size=0!!!');
  result := socketSend(fsocket, buf, size);
  case result of
    0: raise ConnectionClosedException.create('Connection closed (Send = 0)');
    -1: raise ConnectionErrorException.create('Connection error (Send = -1) socketerror:' +
            inttostr(getSocketError));
  end;
end;

function TNormalSocket.getFD: integer; 
begin
  result := fsocket;
end;
  
constructor TNormalSocket.Create(aSocket:TSocket); 
begin
  fsocket := asocket;
  inherited create;
end;

constructor TNormalSocket.Create; 
begin
 fSocket := socketCreate;
 inherited Create;
end;

destructor TNormalSocket.Destroy; 
begin
 socketClose(fsocket);
 inherited;
end;

function TNormalSocket.getAddress: TAddress; 
begin
 result := AddrOfSock(FSocket);
end;

function TNormalSocket.connect(ip:TIpAddress; port:word): boolean; 
begin
  result := socketConnect(fsocket, ip, port);
end;

function TNormalSocket.connect(ip:TIpAddress; port:word; timeout:integer): boolean; 
begin
  result := socketConnect(fsocket, ip, port, timeout);
end;

procedure TNormalSocket.bind(address:TAddress); 
begin
  socketBind(fSocket,address);
end;

procedure TNormalSocket.listen;
begin
  socketListen(fSocket);
end;

function TNormalSocket.accept(var addr:TAddress): TAbstractSocket;
var s: TSocket;
begin
  s := socketAccept(fsocket, addr);
  result := TNormalSocket.create(s);
end;


end.
