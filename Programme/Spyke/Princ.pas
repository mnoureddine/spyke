unit Princ;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ComCtrls, ActnList, Menus,Parametres,Donnee, ExtCtrls,
  IdTCPConnection, IdTCPClient, IdBaseComponent, IdComponent, IdTCPServer,
  ImgList,Utiles, XPMan;

type
  TFormPrinc = class(TForm)
    TreeViewUtilisateurs: TTreeView;
    GroupBox1: TGroupBox;
    GroupBox2: TGroupBox;
    MemoChat: TMemo;
    MemoMessage: TMemo;
    ButtonEnvoyer: TButton;
    GroupBox3: TGroupBox;
    ListBoxJournal: TListBox;
    GroupBox4: TGroupBox;
    LabelIdentifiant: TLabel;
    LabelAdresse: TLabel;
    ActionList1: TActionList;
    MainMenu1: TMainMenu;
    Gnral1: TMenuItem;
    ActionDeconnexion: TAction;
    ActionParametres: TAction;
    ActionQuitter: TAction;
    Dconnexion1: TMenuItem;
    Paramtres1: TMenuItem;
    Quitter1: TMenuItem;
    TimerActualiser: TTimer;
    IdTCPServerSpyke: TIdTCPServer;
    IdTCPClientSpyke: TIdTCPClient;
    ImageList1: TImageList;
    ButtonEnvoyerFichier: TButton;
    ProgressBarFichier: TProgressBar;
    GroupBox5: TGroupBox;
    ButtonConnecter: TButton;
    LabelSelUtilisateur: TLabel;
    LabelSelAdresse: TLabel;
    XPManifest1: TXPManifest;
    procedure ActionParametresExecute(Sender: TObject);
    procedure ActionDeconnexionExecute(Sender: TObject);
    procedure ActionQuitterExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure TimerActualiserTimer(Sender: TObject);
    procedure IdTCPServerSpykeConnect(AThread: TIdPeerThread);
    procedure ButtonEnvoyerClick(Sender: TObject);
    procedure ButtonEnvoyerFichierClick(Sender: TObject);
    procedure IdTCPServerSpykeExecute(AThread: TIdPeerThread);
    procedure IdTCPClientSpykeWorkBegin(Sender: TObject;
      AWorkMode: TWorkMode; const AWorkCountMax: Integer);
    procedure IdTCPClientSpykeWorkEnd(Sender: TObject;
      AWorkMode: TWorkMode);
    procedure IdTCPClientSpykeWork(Sender: TObject; AWorkMode: TWorkMode;
      const AWorkCount: Integer);
    procedure TreeViewUtilisateursClick(Sender: TObject);
    procedure ButtonConnecterClick(Sender: TObject);
    procedure TreeViewUtilisateursChange(Sender: TObject; Node: TTreeNode);
  private
    { D�clarations priv�es }
  public
    { D�clarations publiques }

    procedure ActualiserListe;
    procedure ActualiserSelUtilisateur;
    procedure ConnexionUtilisateur(pUtilisateur: string);
    procedure ChatMessage(Utilisateur: string;pMessage:String);
    procedure ChatDeconnexion;
    procedure JournalChange(Sender : tobject);
  end;

var
  FormPrinc: TFormPrinc;

implementation

{$R *.dfm}

procedure TFormPrinc.ActionParametresExecute(Sender: TObject);
begin
  with TFormParametres.Create(self)do
  begin
      Executer;
      free;
  end;

end;

procedure TFormPrinc.ActionDeconnexionExecute(Sender: TObject);
begin
  Donnees.DeconnexionAnnuaire;
  ChatDeconnexion;
  close;
end;

procedure TFormPrinc.ActionQuitterExecute(Sender: TObject);
begin
  Donnees.DeconnexionAnnuaire;
  ChatDeconnexion;
  Application.Terminate;
end;

procedure TFormPrinc.FormCreate(Sender: TObject);
var
  i : integer;
begin
  Caption := 'Spyke chat v'+GetApplicationVersionString;
  JournalChange(self);
  LabelIdentifiant.Caption := 'Identifiant : '+Donnees.UtilisateurEnCours.Utilisateur;
  LabelAdresse.Caption := 'Adresse : '+Donnees.UtilisateurEnCours.IPPublic+':'+IntToStr(Donnees.UtilisateurEnCours.PortPrive);
  ActualiserListe;
  IdTCPServerSpyke.DefaultPort := Donnees.UtilisateurEnCours.PortPrive;
  IdTCPServerSpyke.Active := true;
  Donnees.OnJournalChange := JournalChange;

end;

procedure TFormPrinc.FormClose(Sender: TObject; var Action: TCloseAction);
begin
    Donnees.DeconnexionAnnuaire;
    Donnees.OnJournalChange := nil;
end;

procedure TFormPrinc.ActualiserListe;
    function UtilisateurConnecte(pUtilisateur : string):boolean;
    var
        i : integer;
    begin
      result := false;
      for i := 0 to Donnees.Nombre -1 do
        if SameText(pUtilisateur,Donnees.Listes[i].Utilisateur) then
        begin
          result := true;
          exit;
        end;
    end;
    function UtilisateurTree(pUtilisateur : string):boolean;
    var
        i : integer;
    begin
      result := false;
      for i := 0 to TreeViewUtilisateurs.Items.Count -1 do
        if pUtilisateur = TreeViewUtilisateurs.Items[i].Text then
        begin
          result := true;
          exit;
        end;
    end;

var
  i : integer;
  Node : TTreeNode;
  Texte : string;
begin



  for i := 0 to Donnees.Nombre -1 do
    if not UtilisateurTree(Donnees.Listes[i].Utilisateur)then
    begin
      Node := TreeViewUtilisateurs.Items.AddChild(nil,Donnees.Listes[i].Utilisateur);
      Node.SelectedIndex :=0;
      Node.ImageIndex :=0;
      Node.StateIndex :=0;
      Donnees.EcrireJournal('Utilisateur '+QuotedStr(Node.Text)+' Connect�.');
    end;

  for i := 0 to TreeViewUtilisateurs.Items.Count -1 do
  begin
    Node := TreeViewUtilisateurs.Items[i];
    if not UtilisateurConnecte(Node.Text) then
    begin
        Donnees.EcrireJournal('Utilisateur '+QuotedStr(Node.Text)+' D�connect�.');
        TreeViewUtilisateurs.Items[i].Delete;
    end;
  end;

end;

procedure TFormPrinc.TimerActualiserTimer(Sender: TObject);
begin

  Donnees.ListeUtilisateurs;
  ActualiserListe;
  ActualiserSelUtilisateur;
end;

procedure TFormPrinc.ConnexionUtilisateur(pUtilisateur: string);
var
  i : Integer;

begin

  ChatDeconnexion;

  for i := 0 to Donnees.Nombre - 1 do
    if SameText(Donnees.Listes[i].Utilisateur,pUtilisateur) then
    begin

      Donnees.UtilisateurConnecte :=Donnees.Listes[i];
      Donnees.EcrireJournal('Connection � l''utilisateur '+Donnees.UtilisateurConnecte.Utilisateur);
      if Donnees.UtilisateurConnecte.IPPublic = Donnees.UtilisateurEnCours.IPPublic then
      begin
          IdTCPClientSpyke.Host := Donnees.UtilisateurConnecte.IPPrive;
          IdTCPClientSpyke.Port := Donnees.UtilisateurConnecte.PortPrive;
      end
      else
      begin
          IdTCPClientSpyke.Host := Donnees.UtilisateurConnecte.IPPublic;
          IdTCPClientSpyke.Port := Donnees.UtilisateurConnecte.PortPublic;
      end;
      Donnees.EcrireJournal('Adresse '+IdTCPClientSpyke.Host+':'+IntToStr(IdTCPClientSpyke.Port));
      IdTCPClientSpyke.Connect(Donnees.Parametres.Connexion.TimeOUT);
      if IdTCPClientSpyke.Connected then
      begin
        Donnees.EcrireJournal('Connexion � l''utilisateur r�ussi.');
        Donnees.UtilisateurActif := true;
        IdTCPClientSpyke.WriteInteger(ChatReqToInt(ChatReqConnexion));
        IdTCPClientSpyke.WriteLn(Donnees.UtilisateurEnCours.Utilisateur);
        IdTCPClientSpyke.WriteLn(Donnees.UtilisateurEnCours.IPPublic);
        IdTCPClientSpyke.WriteInteger(Donnees.UtilisateurEnCours.PortPublic);
        IdTCPClientSpyke.WriteLn(Donnees.UtilisateurEnCours.IPPrive);
        IdTCPClientSpyke.WriteInteger(Donnees.UtilisateurEnCours.PortPrive);
        if ChatReqReussi = ChatReq(IdTCPClientSpyke.ReadInteger)then
        begin
          Donnees.EcrireJournal('Inscription des information r�ussi.');
          ChatMessage(Donnees.UtilisateurConnecte.Utilisateur,'Connexion reussi.')
        end
        else
        begin
          Donnees.EcrireJournal('Inscription des information �chou�.');
          ChatMessage(Donnees.UtilisateurConnecte.Utilisateur,'Connexion �chou�.');
          IdTCPClientSpyke.Disconnect;
          Donnees.UtilisateurActif := false;
        end;
      end;
      exit;
    end;

end;

procedure TFormPrinc.ChatMessage(Utilisateur, pMessage: String);
begin
  MemoChat.Lines.Insert(0,'');
  MemoChat.Lines.Insert(0,pMessage);
  MemoChat.Lines.Insert(0,Utilisateur);

end;

procedure TFormPrinc.IdTCPServerSpykeConnect(AThread: TIdPeerThread);
var

  PortPublic,PortPrive : integer;
  Reussi : boolean;

begin
  Reussi := false;
  if not Donnees.UtilisateurActif then
  begin
    if ChatReq(AThread.Connection.ReadInteger) =  ChatReqConnexion then
    begin
      Donnees.UtilisateurConnecte.Utilisateur := AThread.Connection.ReadLn;
      Donnees.UtilisateurConnecte.IPPublic := AThread.Connection.ReadLn;
      Donnees.UtilisateurConnecte.PortPublic := AThread.Connection.ReadInteger;
      Donnees.UtilisateurConnecte.IPPrive := AThread.Connection.ReadLn;
      Donnees.UtilisateurConnecte.PortPrive := AThread.Connection.ReadInteger;
      Donnees.EcrireJournal('Demande de connexion de l''utilisateur '+Donnees.UtilisateurConnecte.Utilisateur);
      Donnees.EcrireJournal('IP Public '+Donnees.UtilisateurConnecte.IPPublic
                  +':'+IntToStr(Donnees.UtilisateurConnecte.PortPublic));
      Donnees.EcrireJournal('IP Priv� '+Donnees.UtilisateurConnecte.IPPrive
                  +':'+IntToStr(Donnees.UtilisateurConnecte.PortPrive));
      if Donnees.UtilisateurConnecte.IPPublic = Donnees.UtilisateurEnCours.IPPublic then
      begin
          IdTCPClientSpyke.Host := Donnees.UtilisateurConnecte.IPPrive;
          IdTCPClientSpyke.Port := Donnees.UtilisateurConnecte.PortPrive;
      end
      else
      begin
          IdTCPClientSpyke.Host := Donnees.UtilisateurConnecte.IPPublic;
          IdTCPClientSpyke.Port := Donnees.UtilisateurConnecte.PortPublic;
      end;
      Donnees.EcrireJournal('Connexion � l''Adresse '+IdTCPClientSpyke.Host+':'+IntToStr(IdTCPClientSpyke.Port));
      IdTCPClientSpyke.Connect(Donnees.Parametres.Connexion.TimeOUT);
      if IdTCPClientSpyke.Connected then
      begin
        AThread.Connection.WriteInteger(ChatReqToInt(ChatReqReussi));
        Donnees.UtilisateurActif := true;
        Donnees.EcrireJournal('Connexion de l''utilisateur r�ussi.');
        ChatMessage(Donnees.UtilisateurConnecte.Utilisateur,'Connexion reussi');
      end
      else
      begin
        Donnees.EcrireJournal('Connexion de l''utilisateur �chou�.');
        IdTCPClientSpyke.WriteInteger(ChatReqToInt(ChatReqEchoue));
      end;


    end;
  end;


end;

procedure TFormPrinc.ButtonEnvoyerClick(Sender: TObject);
begin
  if Donnees.UtilisateurActif then
  begin
    IdTCPClientSpyke.WriteInteger(ChatReqToInt(ChatReqMessage));
    IdTCPClientSpyke.WriteLn(MemoMessage.Lines.GetText);
    ChatMessage(Donnees.UtilisateurEnCours.Utilisateur,MemoMessage.Lines.GetText);
    MemoMessage.Lines.Clear;


  end;
end;

procedure TFormPrinc.ChatDeconnexion;
var
  i : Integer;
begin
  if  Donnees.UtilisateurActif then
  begin
    IdTCPClientSpyke.WriteInteger(ChatReqToInt(ChatReqDeconnexion));
    IdTCPClientSpyke.Disconnect;
    for i := 0 to IdTCPServerSpyke.Bindings.Count -1 do
        IdTCPServerSpyke.Bindings[i].CloseSocket;
    Donnees.UtilisateurActif := false;
  end;

end;

procedure TFormPrinc.ButtonEnvoyerFichierClick(Sender: TObject);
var
  FileStream : TFileStream;
begin
  if Donnees.UtilisateurActif then
  begin
    with TOpenDialog.Create(self)do
    begin
      if Execute then
      begin
        IdTCPClientSpyke.WriteInteger(ChatReqToInt(ChatReqEnvoyerFichier));
        IdTCPClientSpyke.WriteLn(ExtractFileName(FileName));
        ChatMessage(Donnees.UtilisateurEnCours.Utilisateur,'Demande d''envoie du fichier "'+ExtractFileName(FileName)+'"');
        Donnees.EcrireJournal('Demande d''envoie du fichier "'+ExtractFileName(FileName)+'"');
        if IdTCPClientSpyke.ReadInteger = ChatReqToInt(ChatReqReussi) then
        begin
          FileStream := TFileStream.Create(FileName,fmOpenRead);
          ChatMessage(Donnees.UtilisateurConnecte.Utilisateur,'Acceptation envoie du fichier en cours.');
          Donnees.EcrireJournal('Acceptation envoie du fichier en cours.');
          IdTCPClientSpyke.WriteInteger(FileStream.Size);
          IdTCPClientSpyke.OpenWriteBuffer();
          IdTCPClientSpyke.WriteStream(FileStream);
          IdTCPClientSpyke.CloseWriteBuffer;
          FileStream.Free;
          ChatMessage(Donnees.UtilisateurConnecte.Utilisateur,'Envoie du fichier en termin�.');
          Donnees.EcrireJournal('Envoie du fichier en termin�.');
        end
        else
        begin
          ChatMessage(Donnees.UtilisateurConnecte.Utilisateur,'Refus de reception du fichier.');
          Donnees.EcrireJournal('Refus de reception du fichier.');
        end;


      end;
      free;
    end;

  end;
end;

procedure TFormPrinc.IdTCPServerSpykeExecute(AThread: TIdPeerThread);
var
  Fichier : string;
  FileStream : TFileStream;
  Taille : integer;
begin
  while AThread.Connection.Connected do
  begin
    case ChatReq(AThread.Connection.ReadInteger) of
    ChatReqDeconnexion :
    begin
      Donnees.EcrireJournal('Deconnexion de l''utilisateur.');
     ChatMessage(Donnees.UtilisateurConnecte.Utilisateur,'D�connexion');
     IdTCPClientSpyke.Disconnect;
     Donnees.UtilisateurActif := false;
    end;
    ChatReqMessage :
      ChatMessage(Donnees.UtilisateurConnecte.Utilisateur,AThread.Connection.ReadLn);
    ChatReqEnvoyerFichier :
    begin
      Fichier := AThread.Connection.ReadLn;
      Donnees.EcrireJournal(Donnees.UtilisateurConnecte.Utilisateur+' demande de partage du fichier "'+Fichier+'".');
      ChatMessage(Donnees.UtilisateurConnecte.Utilisateur, 'Demande de partage du fichier "'+Fichier+'".');
      if MessageDlg(Donnees.UtilisateurConnecte.Utilisateur
                      +' Souhaite vous envoyer le fichier "'+Fichier+'",l''acceptez vous ?',mtConfirmation,[mbYes,mbNo],0)=mrYes then
      begin
        with TSaveDialog.Create(self)do
        begin
          FileName := GetCurrentDir+'\'+Fichier;
          if Execute then
          begin
            AThread.Connection.WriteInteger(ChatReqToInt(ChatReqReussi));
            ChatMessage(Donnees.UtilisateurEnCours.Utilisateur, 'Acceptation de reception du fichier en cours.');
            Donnees.EcrireJournal('Acceptation de reception du fichier en cours.');
            FileStream := TFileStream.Create(FileName,fmCreate);
            Taille := AThread.Connection.ReadInteger;
            AThread.Connection.ReadStream(FileStream,Taille);
            FileStream.Free;
            ChatMessage(Donnees.UtilisateurEnCours.Utilisateur, 'Reception du fichier termin�.');
            Donnees.EcrireJournal('Reception du fichier termin�.');
          end
          else
          begin
            AThread.Connection.WriteInteger(ChatReqToInt(ChatReqEchoue));
            ChatMessage(Donnees.UtilisateurEnCours.Utilisateur, 'Refus de reception de fichier.');
            Donnees.EcrireJournal('Refus de reception de fichier.');
          end;

          free;
        end;
      end
      else
      begin
        AThread.Connection.WriteInteger(ChatReqToInt(ChatReqEchoue));
        Donnees.EcrireJournal('Refus de reception de fichier.');
      end;
    end;

    end;

  end;
end;

procedure TFormPrinc.IdTCPClientSpykeWorkBegin(Sender: TObject;
  AWorkMode: TWorkMode; const AWorkCountMax: Integer);
begin
  ProgressBarFichier.Position := 0;
  ProgressBarFichier.Max := AWorkCountMax;

end;

procedure TFormPrinc.IdTCPClientSpykeWorkEnd(Sender: TObject;
  AWorkMode: TWorkMode);
begin
  ProgressBarFichier.Position := 0;
end;

procedure TFormPrinc.IdTCPClientSpykeWork(Sender: TObject;
  AWorkMode: TWorkMode; const AWorkCount: Integer);
begin
  ProgressBarFichier.Position := AWorkCount;
end;

procedure TFormPrinc.TreeViewUtilisateursClick(Sender: TObject);
begin
    ActualiserSelUtilisateur;

end;

procedure TFormPrinc.ActualiserSelUtilisateur;
var
   i : integer;
begin
  ButtonConnecter.Enabled := TreeViewUtilisateurs.Selected <> nil;
  if TreeViewUtilisateurs.Selected <> nil then
  begin
      for i  := 0 to Donnees.Nombre -1 do
      begin
        if SameText(Donnees.Listes[i].Utilisateur,TreeViewUtilisateurs.Selected.Text) then
        begin
          LabelSelUtilisateur.Caption := 'Utilisateur : '+Donnees.Listes[i].Utilisateur;
          LabelSelAdresse.Caption := 'Adresse : '+Donnees.Listes[i].IPPublic +':'+IntToStr(Donnees.Listes[i].PortPublic);
          break;
        end;
      end;
  end
  else
  begin
    LabelSelUtilisateur.Caption := 'Utilisateur : ';
    LabelSelAdresse.Caption := 'Adresse : ';

  end;

end;

procedure TFormPrinc.ButtonConnecterClick(Sender: TObject);
begin
 if TreeViewUtilisateurs.Selected <> nil then
 begin
    ConnexionUtilisateur(TreeViewUtilisateurs.Selected.Text);
 end;

end;

procedure TFormPrinc.TreeViewUtilisateursChange(Sender: TObject;
  Node: TTreeNode);
begin
  ButtonConnecter.Enabled := TreeViewUtilisateurs.Selected <> nil;
end;

procedure TFormPrinc.JournalChange(Sender: tobject);
begin
  ListBoxJournal.Items.Assign(Donnees.Journal);
end;

end.
