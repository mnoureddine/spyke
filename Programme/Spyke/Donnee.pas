unit Donnee;

interface

uses
  SysUtils, Classes,IniFiles, IdTCPConnection, IdTCPClient,
  IdBaseComponent, IdComponent, IdTCPServer,Annuaire,upnpunit;

type
  TChatReq = (ChatReqConnexion,ChatReqDeconnexion,ChatReqMessage,ChatReqReussi,ChatReqEchoue,ChatReqEnvoyerFichier);
  TUtilisateur =  record
      Utilisateur : string;
      IPPrive : string;
      IPPublic : string;
      PortPublic : integer;
      PortPrive : integer;
    end;

  TDonnees = class(TDataModule)
    IdTCPClientAnnuaire: TIdTCPClient;
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
  private
    { D�clarations priv�es }
  public
    { D�clarations publiques }
    FichierIni : TIniFile;
    Parametres : record
      General : record

      end;
      Connexion : record
        Utilisateur : string;
        MotPasse : string;
        Annuaire : string;
        PortAleatoire : boolean;
        PortLocal : integer;
        ConnexionAuto : boolean;
        TimeOUT : integer;
      end;
    end;

    UtilisateurEnCours : TUtilisateur;
    Listes : array[0..10] of TUtilisateur;
    Nombre : integer;
    UtilisateurConnecte : TUtilisateur;
    UtilisateurActif : boolean;
    UpnpServices : TUpnpServices;
    Journal : TStrings;
    OnJournalChange : TNotifyEvent;
    procedure ChargerParametres;
    procedure EnregistrerParametres;
    function ConnexionAnnuaire(pUtilisateur: String;pMotPasse : string) : boolean;
    procedure DeconnexionAnnuaire;
    procedure ListeUtilisateurs;
    procedure EcrireJournal(pMessage : string);
    procedure UpnpServicesLog(Sender : TObject;pLog:string);



  end;

const
  PORT_ANNURAIRE = 4523;

  FICHIER_PARAMETRES = 'spyke.ini';
  SPYKE_ID =   'Spyke Chat';
  SPYKE_PROTOCOLE = 'TCP';
var
  Donnees: TDonnees;

function ChatReqToInt(pChatReq : TChatReq):integer;
function ChatReq(Req : Integer) : TChatReq;

implementation

{$R *.dfm}

{ TDonnees }
function ChatReqToInt(pChatReq : TChatReq):integer;
begin
  Result := Integer(pChatReq);
end;

function ChatReq(Req : Integer) : TChatReq;
begin
  result := TChatReq(Req);
end;


procedure TDonnees.ChargerParametres;
begin
  with Parametres do
  begin
    Connexion.Utilisateur := FichierIni.ReadString('Connexion','Utilisateur','');
    Connexion.MotPasse := FichierIni.ReadString('Connexion','MotPasse','');
    Connexion.Annuaire := FichierIni.ReadString('Connexion','Annuaire','mehiradev.dyndns.org');
    Connexion.PortAleatoire := FichierIni.ReadBool('Connexion','PortAleatoire',false);
    Connexion.PortLocal := FichierIni.ReadInteger('Connexion','PortLocal',5231);
    Connexion.ConnexionAuto := FichierIni.ReadBool('Connexion','ConnexionAuto',false);
    Connexion.TimeOUT := FichierIni.ReadInteger('Connexion','TimeOUT',5000);
  end;
end;

procedure TDonnees.EnregistrerParametres;
begin
  with Parametres do
  begin
    FichierIni.WriteString('Connexion','Utilisateur',Connexion.Utilisateur );
    FichierIni.WriteString('Connexion','MotPasse',Connexion.MotPasse);
    FichierIni.WriteString('Connexion','Annuaire',Connexion.Annuaire);
    FichierIni.WriteBool('Connexion','PortAleatoire',Connexion.PortAleatoire);
    FichierIni.WriteInteger('Connexion','PortLocal',Connexion.PortLocal);
    FichierIni.WriteBool('Connexion','ConnexionAuto',Connexion.ConnexionAuto);

  end;
end;

procedure TDonnees.DataModuleCreate(Sender: TObject);
begin

  FichierIni := TIniFile.Create(FICHIER_PARAMETRES);
  ChargerParametres;
  UpnpServices := TUpnpServices.create;
  UpnpServices.OnLog := UpnpServicesLog;
  Journal := TStringList.Create;
  OnJournalChange := nil;
end;

function TDonnees.ConnexionAnnuaire(pUtilisateur: String;pMotPasse : string) : boolean;
var
  i : integer;

begin

  result := false;
  EcrireJournal('Initialisation du service NAT-PMP.');
  if UpnpServices.initialize then
  begin
    UtilisateurEnCours.IPPublic := trim(UpnpServices.PublicIp);
    UtilisateurEnCours.IPPrive := trim(UpnpServices.PrivateIP);
    for i := 0 to 20 do
    begin
      if UpnpServices.openFirewall(Parametres.Connexion.PortLocal,SPYKE_PROTOCOLE,SPYKE_ID) then
        break
      else
        Inc(Parametres.Connexion.PortLocal);

    end;
    UtilisateurEnCours.PortPublic := Parametres.Connexion.PortLocal;
    UtilisateurEnCours.PortPrive := Parametres.Connexion.PortLocal;

    EcrireJournal('IP Public '+UtilisateurEnCours.IPPublic+':'+IntToStr(UtilisateurEnCours.PortPublic));
    EcrireJournal('IP Priv� '+UtilisateurEnCours.IPPrive+':'+IntToStr(UtilisateurEnCours.PortPrive));
    EcrireJournal('Connexion � l''annuaire '+Parametres.Connexion.Annuaire+':'+IntToStr(PORT_ANNURAIRE));
  end;
  IdTCPClientAnnuaire.Host := Parametres.Connexion.Annuaire;
  IdTCPClientAnnuaire.Port := PORT_ANNURAIRE;
  IdTCPClientAnnuaire.Connect(Parametres.Connexion.TimeOUT);
  if IdTCPClientAnnuaire.Connected then
  begin
    EcrireJournal('Connexion � l''annuaire r�ussi.');
    IdTCPClientAnnuaire.WriteInteger(AnnuaireReqToInt(AnnReqConnexion));
    IdTCPClientAnnuaire.Writeln(pUtilisateur);
    IdTCPClientAnnuaire.Writeln(pMotPasse);
    IdTCPClientAnnuaire.Writeln(UtilisateurEnCours.IPPublic);
    IdTCPClientAnnuaire.WriteInteger(UtilisateurEnCours.PortPublic);
    IdTCPClientAnnuaire.Writeln(UtilisateurEnCours.IPPrive);
    IdTCPClientAnnuaire.WriteInteger(UtilisateurEnCours.PortPrive);

    if AnnuaireReq(IdTCPClientAnnuaire.ReadInteger)= AnnReqReussi then
    begin
        UtilisateurEnCours.Utilisateur := pUtilisateur;
        Result := true;
        EcrireJournal('Inscription r�ussi.');
    end
    else
      IdTCPClientAnnuaire.Disconnect;

  end;
end;


procedure TDonnees.DeconnexionAnnuaire;
begin
  if IdTCPClientAnnuaire.Connected then
  begin
    IdTCPClientAnnuaire.WriteInteger(AnnuaireReqToInt(AnnReqDeconnexion));
    IdTCPClientAnnuaire.WriteLn(UtilisateurEnCours.Utilisateur);
    IdTCPClientAnnuaire.Disconnect;
    UpnpServices.closeFirewall(UtilisateurEnCours.PortPrive,SPYKE_PROTOCOLE);
    EcrireJournal('D�connexion de l''annuaire r�ussi.');
  end;

end;

procedure TDonnees.ListeUtilisateurs;
var
  i, nb : integer;
begin
  if IdTCPClientAnnuaire.Connected then
  begin
    IdTCPClientAnnuaire.WriteInteger(AnnuaireReqToInt(AnnReqListes));
    IdTCPClientAnnuaire.WriteLn(UtilisateurEnCours.Utilisateur);
    Nombre := IdTCPClientAnnuaire.ReadInteger();
    for i := 0 to Nombre -1 do
    begin
      Listes[i].Utilisateur := IdTCPClientAnnuaire.ReadLn;
      Listes[i].IPPublic := IdTCPClientAnnuaire.ReadLn;
      Listes[i].PortPublic := IdTCPClientAnnuaire.ReadInteger;
      Listes[i].IPPrive := IdTCPClientAnnuaire.ReadLn;
      Listes[i].PortPrive := IdTCPClientAnnuaire.ReadInteger;

    end;
  end;

end;


procedure TDonnees.DataModuleDestroy(Sender: TObject);
begin
    UpnpServices.Free;
    Journal.Free;
    OnJournalChange := nil;
end;

procedure TDonnees.EcrireJournal(pMessage: string);
begin
  Journal.Insert(0, DateTimeToStr(now)+#9+pMessage);
  if Assigned(OnJournalChange) then
    OnJournalChange(Self);
end;

procedure TDonnees.UpnpServicesLog(Sender: TObject; pLog: string);
begin
  EcrireJournal('UpnpServices Log : '+pLog);
end;

end.
