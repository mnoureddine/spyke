
unit Utiles;

interface

uses
    Graphics,Forms,Windows,SysUtils,idHash,IdHashMessageDigest;
    
function GetApplicationVersionString : string;
function MD5CryptMotDePasse(const MotDePasse : string) : String;


implementation

function GetApplicationVersionString : string;
var
    VerSize : Dword;
    Zero : THandle;
    PBlock : Pointer;
    PS : Pointer;
    Size : Uint;
begin
    result := '0.0.0.0';
    VerSize := GetFileVersionInfoSize(PChar(Application.ExeName),Zero);
    if VerSize = 0 then exit;
    getMem(PBlock,VerSize);
    GetFileVersionInfo(pChar(Application.ExeName),0,VerSize,PBlock);
    VerQueryValue(PBlock,pChar('\\StringFileInfo\\040C04E4\\FileVersion'),PS,Size);
    Result := StrPas(PS);
end;

function MD5CryptMotDePasse(const MotDePasse : string) : String;
var
  idmd5 : TIdHashMessageDigest5;
begin

  idmd5 := TIdHashMessageDigest5.Create;
  try
    result := LowerCase(idmd5.AsHex(idmd5.HashValue(MotDePasse)));
  finally
    idmd5.Free;
  end;
end;

end.
