object FormConnexion: TFormConnexion
  Left = 423
  Top = 254
  BorderStyle = bsDialog
  Caption = 'Connexion'
  ClientHeight = 241
  ClientWidth = 381
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'Arial'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 16
  object ButtonQuitter: TButton
    Left = 288
    Top = 200
    Width = 83
    Height = 33
    Cancel = True
    Caption = 'Quitter'
    ModalResult = 2
    TabOrder = 2
    OnClick = ButtonQuitterClick
  end
  object ButtonConnexion: TButton
    Left = 200
    Top = 200
    Width = 83
    Height = 33
    Caption = 'Connexion'
    Default = True
    ModalResult = 1
    TabOrder = 1
    OnClick = ButtonConnexionClick
  end
  object GroupBox1: TGroupBox
    Left = 16
    Top = 8
    Width = 353
    Height = 185
    Caption = 'Identification'
    TabOrder = 0
    object Label1: TLabel
      Left = 141
      Top = 127
      Width = 67
      Height = 16
      Caption = 'Port local : '
      FocusControl = SpinEditPortLocal
    end
    object LabeledEditUtilisateur: TLabeledEdit
      Left = 144
      Top = 24
      Width = 185
      Height = 24
      EditLabel.Width = 70
      EditLabel.Height = 16
      EditLabel.Caption = 'Utilisateur : '
      LabelPosition = lpLeft
      TabOrder = 0
    end
    object LabeledEditMotPasse: TLabeledEdit
      Left = 144
      Top = 56
      Width = 185
      Height = 24
      EditLabel.Width = 91
      EditLabel.Height = 16
      EditLabel.Caption = 'Mot de passe : '
      LabelPosition = lpLeft
      PasswordChar = '*'
      TabOrder = 1
    end
    object LabeledEditAnnuaire: TLabeledEdit
      Left = 144
      Top = 88
      Width = 185
      Height = 24
      EditLabel.Width = 63
      EditLabel.Height = 16
      EditLabel.Caption = 'Annuaire : '
      LabelPosition = lpLeft
      TabOrder = 2
      Text = 'mehiradev.dyndns.org'
    end
    object SpinEditPortLocal: TSpinEdit
      Left = 208
      Top = 120
      Width = 121
      Height = 26
      MaxValue = 0
      MinValue = 0
      TabOrder = 3
      Value = 5231
    end
    object CheckBoxPortAleatoire: TCheckBox
      Left = 24
      Top = 128
      Width = 97
      Height = 17
      Alignment = taLeftJustify
      Caption = 'Port al'#233'atoire'
      TabOrder = 4
      OnClick = CheckBoxPortAleatoireClick
    end
    object CheckBoxConnexionAuto: TCheckBox
      Left = 72
      Top = 160
      Width = 169
      Height = 17
      Alignment = taLeftJustify
      Caption = 'Connexion automatique : '
      TabOrder = 5
    end
  end
end
